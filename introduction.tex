%!TEX root = thesis.tex

\chapter*{Introduction}
\addcontentsline{toc}{chapter}{Introduction}

Since the early 1980s, it has been argued \autocite{feynman1982simulating} that simulating quantum systems is a very challenging task.
% TWO MAIN FACTORS MAKING QUANTUM SYSTEMS HARD TO SIMULATE
One source of difficulty is the number of parameters needed to characterize a generic quantum system, which grows exponentially with the size of the system.
This means that even only \emph{storing} the state of a large quantum system is not feasible with classical computer memories.
Furthermore, the number of operations needed to simulate the temporal evolution of such a system also scales exponentially with the size.
Thus, the only way to avoid this exponential overhead in the evolution is the use of approximation methods (such as Monte Carlo methods).
However, for many problems of interest, no good approximation scheme are available.
It is then still an open problem whether they can be efficiently simulated with a classical approach.
% IT IS NOT KNOWN WHETHER QUANTUM SYSTEMS CAN BE EFFICIENTLY SIMULATED WITH CLASSICAL COMPUTERS
Hence, it is widely accepted that \emph{classical systems cannot in general efficiently simulate quantum systems}.
While it is not yet possible to prove it, neither mathematically nor experimentally, there are strong evidences to believe that this is the case.

This distinction between classical and quantum world has many implications.
One of the most notable examples concerns the possibility that computers exploiting the weirdnesses of quantum mechanics may be able to carry out computations impossible with only classical resources.
% IT IS NOT THAT EASY TO EXPERIMENTALLY OBSERVE THE QUANTUM ADVANTAGE, AND AARONSON THE SAVIOUR
With the current available technologies, the experimental observation of this \emph{quantum advantage} (sometimes referred to as \emph{quantum supremacy}~\cite{preskill2012,preskill2012blog}) has proven itself to be rather difficult to achieve. 
In particular, to observe a post-classical computation with a universal quantum computer one first needs to solve the problem of \emph{fault-tolerant quantum computation}~\cite{preskill1998fault}, which is known to be possible in principle~\cite{knill1998resilient,aharonov2008fault,knill2001scheme}, but might require decoherence rates that are several orders of magnitude below what achievable today.
In the case of linear optics, a number of no-go theorems led to the widespread  belief that linear interferometry alone could not provide a path to universal quantum computation.
For this reason the result of \ac{AA}, that passive linear optical interferometers with many-photon inputs cannot be efficiently simulated by a classical computer~\cite{aaronson2011computational}, represented a significant advance.
The related computational problem, that is, sampling from the output probability distribution of such an apparatus, was named by \ac{AA} the \textsc{BosonSampling} problem.
A quantum device able to efficiently solve it is referred to as a \emph{boson sampler}.

%% AA RESULT
More in detail, the \BosonSampling{} computational problem consists in sampling from the output probability distribution resulting from the time-evolution of $n$ indistinguishable photons into a random $m\times m$ unitary transformation.
The hardness of \BosonSampling{} arises from the fact that the scattering amplitude between an input and an output state configuration is proportional to the \emph{permanent} of a suitable \dimSq{n} matrix, where the \emph{permanent} is a particular function, defined similarly to the determinant, which in the general case is known to be hard to compute classically.
This immediately suggests an experimental scheme to build a boson sampler using only linear optical elements: just inject $n$ indistinguishable photons into an appropriate linear optical interferometer, and use \emph{photon-counting detectors} to detect the resulting output states.
\ac{AA} showed that, already with $20 < n < 30$ and $m \gg n$, this would provide direct evidence that a quantum computer can solve a problem faster than what is possible with any classical device.
While this regime is far from our current technological capabilities, several implementations of 2- and 3-photon devices    have soon been reported \cite{broome2013photonic,crespi2013integrated,tillmann2013experimental,spring2013boson}, and other more complex implementations followed \cite{spagnolo2013three,spagnolo2014experimental,carolan2014experimental,carolan2014verifying,carolan2015universal,bentivegna2015experimental}.

However, the originally proposed scheme to implement \BosonSampling{}, that is, to generate the input $n$-photon state through \ac{SPDC}, suffers from scalability problems.
Indeed, it is unfeasible to generate high numbers of indistinguishable input photons with this method, due to the generation probability decreasing exponentially with $n$.
For this reason, an alternative scheme, named \emph{scattershot boson sampling} \cite{aaronson2013new}, has been devised \cite{lund2014boson}, and subsequently implemented \cite{bentivegna2015experimental}.
Contrarily to a classical boson sampler, a scattershot boson sampler uses $m$ \ac{SPDC} sources, one per input mode of the interferometer, to generate random (but known) $n$-photon input states, with $n \ll m$.
Each \ac{SPDC} source generates a pair of photons, one of which is injected into the interferometer, while the other is used to \emph{herald} the \ac{SPDC} generation event.
The use of a scattershot boson sampling scheme results in an exponential increase of the probability of generating $n$ indistinguishable photons, for $m$ and $n$ large enough.

While the key part of \BosonSampling{} resides in its simulation complexity, this very hardness also poses a problem of \emph{certification} of such a device.
Indeed, it is believed \cite{aaronson2011computational} that, when $n$ is large enough, a classical computer cannot even \emph{verify} that the device is solving \BosonSampling{} correctly.
However, it is still possible to obtain circumstantial evidence of the correct functioning of a device, and efficiently distinguish the output of a boson sampler from that resulting from alternative probability distributions, like the output produced by classical particles evolving through the same interferometer.

A number of validation schemes were subsequently devised to validate the output resulting from true many-boson interference \cite{aaronson2014bosonsampling,spagnolo2014experimental,carolan2014verifying,tichy2014stringent,crespi2015suppression}.
In particular, the tests currently more suitable to identify true many-body interference \cite{tichy2014stringent} are those based on \acp{ZTL} \cite{tichy2010zero}.
A \ac{ZTL}, also often referred to as \emph{suppression law}, is a rule which, for certain particular unitary evolutions, is able to predict that the probability of certain input-output configurations is exactly zero, \emph{without having to compute any permanent}.

However, for the current validation schemes based on \acp{ZTL}, it is mandatory that the input states possess particular symmetries.
This requirement may thus be an issue when the \acp{ZTL} are applied to validate a scattershot boson sampler.
Indeed, the input state in scattershot boson sampling is not fixed, but changes randomly at each $n$-photon generation event.
This mandates for a new \ac{ZTL}-based validation scheme to be devised, able to efficiently validate a scattershot boson sampling experiment, but still keeping the capability of distinguishing alternative probability distributions.

In this thesis we report on both theoretical and experimental advances in the context of validating classical and scattershot boson sampling experiments:
\begin{itemize}
    \item  From the theoretical point of view, we devise a new validation scheme, more suitable to validate scattershot boson sampling experiments.
    This scheme, based on a \ac{ZTL} valid for a particular class of matrices, the so-called \emph{Sylvester matrices}, generalizes the \ac{ZTL} reported in \cite{crespi2015suppression}, presenting significantly higher predictive capabilities.
    \item From the experimental point of view, we report on the experimental implementation \cite{crespi2015quantum} of the validation scheme proposed in \cite{tichy2014stringent} based on the \ac{ZTL} for Fourier matrices~\cite{tichy2010zero}.
    To this end, a scalable methodology to implement the Fourier transformation on integrated photonics was adopted.
    This approach exploits the 3-D capabilities of femtosecond laser writing technique, together with a recently proposed \cite{barak2007quantum} quantum generalization of the Fast Fourier transform algorithm \cite{cooley1965algorithm}, which allows a significant improvement in the number of elementary optical elements required to implement the desired Fourier transformation.
\end{itemize}

The thesis is structured as follows:
\Cref{chapter:foundations} opens with a brief survey of classical and quantum computer science.
In \cref{chapter:quantumAndNonlinearOptics}, after a brief exposition of the theoretical formalism for many-body quantum states, the fundamental tools used in quantum optics experiments are presented.
In \cref{chapter:bosonSampling} the \BosonSampling{} problem is introduced.
The problem of scaling boson sampling experiments is discussed, together with the recently proposed alternative scheme named \emph{scattershot boson sampling}.
In \cref{chapter:bosonSamplingValidation} the subject of boson sampling validation is introduced, and an outline of the proposed solutions is provided.
In particular, the focus is on the validation schemes based on zero-transmission laws for Fourier and Sylvester matrices, and the possibility of applying them to scattershot boson sampling experiments.
In \cref{chapter:theoreticalResults} we present a new zero-transmission law for Sylvester matrices.
Exploiting this zero-transmission law, we present a scheme to validate scattershot boson sampling experiments.
The thesis closes with \cref{chapter:experimentalResults}, where we present the experimental implementation of a validation scheme for Fourier matrices.
The experimental and technological aspects of the experiment are discussed, from the femtosecond laser-written technology employed to build the integrated interferometers, to a novel method to efficiently implement the Fourier transform on an integrated photonics chip.
A full analysis of the chip reconstruction and the observed suppression effects follows.
The chapter closes with a discussion of the usefulness of the presented work, and the possible future improvements.

The work presented in this thesis was carried out at the Quantum Information Lab, University of Rome \emph{La Sapienza}.