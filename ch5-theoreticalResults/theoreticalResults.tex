%!TEX root = ..\thesis.tex

As discussed in \cref{sec:SylvesterSuppressionLaw}, the suppression law given in \cref{thm:sylvester_suppression_law} \cite{crespi2015suppression} is not suitable to validate scattershot boson sampling experiments.
On the other hand, as shown in \cref{fig:fourierVsSylvesterSuppPerInput,fig:fourierVsSylvesterM16N8}, the actual fraction of suppressed pairs is significanly higher, and some kind of suppression law seems to hold for every input state, not just for those considered by \cref{thm:sylvester_suppression_law}.

In this chapter we propose a generalization of \cref{thm:sylvester_suppression_law}, which predicts a much higher fraction of suppressed pairs, and is thus more suitable to validate scattershot boson sampling experiments.
The proposed suppression law is described in \cref{sec:ch5:mainResults}, using notations and lemmas given in \cref{sec:ch5:preliminaries}.
In \cref{sec:ch5:BayesianValidationScheme} we discuss a validation scheme to distinguish quantum many-boson interference (up to small errors) from the output resulting from distinguishable photons, by exploiting the proposed suppression law.
In this context we also derive an analytic expression for the mean number of samples necessary to experimentally distinguish a \textit{nearly} quantum evolution from the classical hypothesis.
The chapter closes with a summary of the results in \cref{sec:ch5:conclusions}.

\section{Preliminaries}
\label{sec:ch5:preliminaries}

For the results presented in this chapter we will introduce some new notations, on top of those given in \cref{def:MOLandMAL}:
\begin{definition}[Binary Matrix representation]
	\label{def:binaryMatrixRep}
	Let $\textbf r=(r_1,\dots,r_m)$ be the \ac{MOL} of a state of $n$ particles in $m=2^p$ modes, for some positive integer $p$,
	and let $\textbf R=(R_1,\dots,R_n)$ be the corresponding \ac{MAL}.
	We denote with $R^{(2)}_i$ the binary representation of $R_i-1$,
	padded with zeros on the left so as to make it a vector of length $p$,
	and with $R_{i,\alpha}$ the $\alpha$-th element of $R^{(2)}_i$.
	This construction allows us to represent a many-body state $\textbf r$ as a \ac{BM} $R$, i.e. as an $n\!\times\! p$-dimensional matrix whose elements are either 0 or 1.
	We will refer to $R$ as the \ac{BM} representation (or simply the \ac{BM}) of the state $\textbf{r}$.
\end{definition}
\begin{remark}
	We note that for indistinguishable particles it is not meaningful to assign a specific mode to a particle.
	This means that the order of the elements of the \ac{MAL} describing a state of many indistinguishable particles is not relevant.
	In other words, if two \acp{MAL} $\textbf R$ and $\textbf R'$ differ only for the order of their elements, they represent the same physical state.
	Similarly, if two binary matrices $R$ and $R'$ can be obtained one from the other with only a reordering of the rows, they represent the same physical state.
\end{remark}

\begin{example}
    Consider the case $m=4$ and $n=2$, and the state with \ac{MOL} $\textbf r \equiv (0,1,1,0)$
    and corresponding \ac{MAL} $\textbf R = (2,3)$.
    Following the notations introduced above we will have
    $\textbf R_1=(0,1)$, $\textbf R_2=(1,0)$,
    and the \ac{BM} of $\textbf r$ will be the $2\!\times\!2$ matrix
    $R = \begin{pmatrix} 0&1\\1&0 \end{pmatrix}$.
\end{example}

\begin{example}
    Consider the case $m=8$, $n=4$, and the state with \ac{MOL}\\$\textbf s \equiv (1,1,1,0,0,0,0,1)$
    and corresponding \ac{MAL} $\textbf S = (1,2,3,8)$.
    Following the notations introduced above we will have
    $\textbf S_1=(0,0,0)$, $\textbf S_2=(0,0,1)$, $\textbf S_3=(0,1,0)$, $\textbf S_4=(1,1,1)$,
    and the \ac{BM} representation of $\textbf r$ will be the $4\!\times\!3$ matrix
    \begin{equation*}
        S = \begin{pmatrix} 0&0&0\\0&0&1\\0&1&0\\1&1&1 \end{pmatrix}.
    \end{equation*}
\end{example}

\begin{definition}
	Let $R$ be an \dimens{n}{p} \ac{BM}, and let $A$ be a subset of the $p$ columns of $R$.
	Then we denote with $\mathcal N^A(R)$ the matrix obtained by negating the columns of $R$ specified in $A$.
	In other words, expliciting the indices, $\mathcal N^A(R)$ is defined as
	\begin{equation}
		[\mathcal N^A(R)]_{k,\alpha} \equiv
		\begin{cases}
			R_{k,\alpha}\oplus 1, & \text{for }\alpha \in A,\\
			R_{k,\alpha}, & \text{for }\alpha \notin A.
		\end{cases}
	\end{equation}
	where $\oplus$ is the sum modulo 2.\\
	Similarly, if $\textbf r$ is a \ac{MOL} representing some state of $m=2^p$ modes,
	we denote with $\mathcal N^A(\mathbf r)$ the \ac{MOL} obtained applying to $\mathbf r$ the following three operations:
	\begin{itemize}
		\item Convert $\textbf r$ to the binary matrix $R$,
		\item Negate the columns of $R$ specified in $A$, obtaining the binary matrix $\mathcal N^A(R)$,
		\item Find the \ac{MOL} corresponding to the binary matrix $\mathcal N^A(R)$.
	\end{itemize}
	%Note that while we always have $\mathcal N^A(R) \neq R$ for any BM $R$,
	%it can happen for some MOLs $\mathbf r$ to have $\mathcal N^A(\mathbf r)=\mathbf r$.
	\label{def:negationOfColumns}
\end{definition}

\begin{example}
	\label{eg:11110000}
	Consider the case $m=8$, $n=4$ and the state with \ac{MOL} $\textbf r=(1,1,1,1,0,0,0,0)$,
	corresponding to the \ac{MAL} $\textbf R=(1,2,3,4)$,
	and expressed in the \ac{BM} representation as
	\begin{equation*}
		R = \begin{pmatrix} 0&0&0\\0&0&1\\0&1&0\\0&1&1 \end{pmatrix}.
	\end{equation*}
	Then, following the notations introduced above, we have when $A$ consists of a single element,
	\begin{equation*}
		\mathcal N^{\{1\}}(R) = \begin{pmatrix} 1&0&0\\1&0&1\\1&1&0\\1&1&1 \end{pmatrix},
		\quad
		\mathcal N^{\{2\}}(R) = \begin{pmatrix} 0&1&0\\0&1&1\\0&0&0\\0&0&1 \end{pmatrix},
		\quad
		\mathcal N^{\{3\}}(R) = \begin{pmatrix} 0&0&1\\0&0&0\\0&1&1\\0&1&0 \end{pmatrix}.
	\end{equation*}
	From this we see that $\mathcal N^{\{2\}}(R)$ and $\mathcal N^{\{3\}}(R)$ are equal to $R$,
	up to a permutation of the rows,
	and they therefore represent the same physical state.
	On the other hand, $\mathcal N^{\{1\}}(R)$ cannot be made equal to $R$ only rearranging its rows, and therefore represents a different state.
	In other words, we have:
	\begin{equation*}
		\mathcal N^{\{1\}}(\mathbf r) = (0,0,0,0,1,1,1,1) \neq \mathbf r,
		\qquad
		\mathcal N^{\{2\}}(\mathbf r) = \mathbf r,
		\qquad
		\mathcal N^{\{3\}}(\mathbf r) = \mathbf r.
	\end{equation*}
	On the other hand, the set $A$ can contain more than a single element. For example:
	\begin{equation*}
		\mathcal N^{\{1,3\}}(R) = \begin{pmatrix} 1&0&1\\1&0&0\\1&1&1\\1&1&0 \end{pmatrix},
		\qquad
		\mathcal N^{\{1,2,3\}}(R) = \begin{pmatrix} 1&1&1\\1&1&0\\1&0&1\\1&0&0 \end{pmatrix},
	\end{equation*}
	which, for the same reasons, satisfy
	\begin{equation*}
		\mathcal N^{\{1,3\}}(\mathbf r) = (0,0,0,0,1,1,1,1) \neq \mathbf r,
		\qquad
		\mathcal N^{\{1,2,3\}}(\mathbf r) = (0,0,0,0,1,1,1,1) \neq \mathbf r.
	\end{equation*}
\end{example}

The following results will also be useful in the following:
\begin{lemma}
	For any two integers $a$ and $b$
	%, denoting with $a\oplus b$ the sum modulo 2 of $a$ and $b$,
	%i.e. $a\oplus b\equiv \mod\!(a+b,2)$,
	we have
	\begin{equation}
		(-1)^{a+b} = (-1)^{a \oplus b}.
	\end{equation}
	More generally, for any finite set of integer numbers $\{a_1,\dots,a_N\}$, we have
	\begin{equation}
		(-1)^{\sum_{k=1}^N a_k} = (-1)^{\bigoplus_{k=1}^N a_k}.
	\end{equation}
	\label{lemma:sumModulo2AtExponent}
\end{lemma}

\begin{lemma}
	%Let $n$ be a positive integer,
	%denote with $S_n$ the set of permutations of the set
	%$\{1,\dots,n\}$ (equivalently, $S_n$ is the set of bijections of $\{1,\dots,n\}$ in itself),
	Let $S_n$ be the set of permutations of $\{1,\dots,n\}$,
	and let $\tau \in S_n$ be a permutation different from the identity
	such that $\tau^2 = \mathbb 1$
	(i.e. $\tau(\tau(k))=k$ for each $k\in \{1,\dots,n\}$).
	Then we can univocally associate to each $\sigma \in S_n$ another (different)
	permutation $\sigma_\tau \equiv\tau \circ \sigma$, where $\circ$ denotes the composition of permutations.
	\label{lemma:partitionOfSetOfPermutations}
\end{lemma}

\begin{lemma}
	%Let $R$ be the binary matrix corresponding to some state $\mathbf r$ of $n$ particles in $m=2^p$ modes, for some positive integer $p$,
	%and let $\mathcal N^A$ be the operation defined in \cref{def:negationOfColumns}.
	%Then
	Let $R$ and $\mathcal N^A$ be as in \cref{def:MOLandMAL,def:negationOfColumns}, respectively. Then
	\begin{itemize}
		\item $\mathcal N^A(\mathbf r) = \mathbf r$ if and only if
			$\mathcal N^A(R)=R_\tau$ for some permutation $\tau \in S_n$,
			where with $R_\tau$ we denote the matrix obtained from $R$
			permuting the rows according to $\tau$, i.e.
			$(R_\tau)_{k,\alpha} \equiv R_{\tau(k),\alpha}$ for each $k=1,\dots,n$.
		\item If $\tau$ is such that $\mathcal N^A(R)=R_\tau$, then
			\begin{enumerate}
				\item $\tau^2=\mathbb 1$, 
				\item $\tau(k) \neq k$ for each $k=1,\dots,n$,
				\item for each $\sigma \in S_n$, $\,\,\,\mathcal N^A(R_\sigma) = R_{\tau \circ \sigma}$,
				\item for all columns $\alpha \in A$, $\,R^\alpha$ will have an equal number of 1s and 0s,
				\item for all columns $\alpha \notin A$, $\,R^\alpha$ will have an \textit{even} number of 1s, \textit{and} an even number of 0s.
			\end{enumerate}
	\end{itemize}
	\label{lemma:implicationsOfNeutralNegation}
\end{lemma}

\section{Main results}
\label{sec:ch5:mainResults}

Using the lemmas and definitions listed above,
we are now ready to state the main result of the section:
\begin{theorem}[Sufficient condition for suppressed pairs]
	Let $\textbf r$ and $\textbf s$ be two many-boson states of $n$ particles in $m=2^p$ modes,
	let $R$ and $S$ be the corresponding BM representations,
	and let $A$ be a subset of the columns of $R$.
	If the following conditions
	\begin{subequations}
	  \label{eq:suppressionCondition}
	  \begin{empheq}[left=\empheqlbrace]{align}
		& \mathcal N^A(\textbf r) = \textbf r, \label{eq:suppressionConditionForR}\\
		& \displaystyle\bigoplus_{k=1}^n \bigoplus_{\alpha \in A} S_{k\alpha} = 1
		\label{eq:suppressionConditionForS}.
	  \end{empheq}
	\end{subequations}
	are simultaneously satisfied, then
	$\mathcal A(\mathbf r, \mathbf s, U^S) = \mathcal A(\mathbf s, \mathbf r, U^S) = 0$.

	The converse does not in general hold.
	In other words, this condition is \emph{sufficient} but not \emph{necessary} for a pair to be suppressed.
	\label{thm:mySuppressionLaw}
\end{theorem}

\begin{proof}
	From now on, we will use the abbreviation
	$\mathcal A \equiv \mathcal A(\mathbf r, \mathbf s, U^S)$,
	leaving implicit the dependence on the states and the unitary evolution considered.
	The transition amplitude $\mathcal A$ can be expressed in terms of a permanent of the form
	\begin{equation}
	  \mathcal A =
	  \frac{1}{\sqrt{r_1!\dots r_m! s_1!\dots s_m!}}
	  \operatorname{perm} \left(U^S[\Rstate | \Sstate ] \right),
	  \label{eq:scatteringAmplitudeWithPermanents}
	\end{equation}
	where $\operatorname{perm}(U)$ denotes the permanent of $U$,
	and the notation $U^S[\Rstate | \Sstate ] $ was given in \cref{def:submatrices}.
	
	Using \cref{def:sylvesterMatrices,eq:sylvesterMatrixElements},
	we get from \cref{eq:scatteringAmplitudeWithPermanents}
	\begin{equation}
		\mathcal{A} =
		C \sum_{\sigma \in S_n} \prod_{k=1}^n
		(-1)^{R_{\sigma(k)} \odot S_k}
		= C \sum_{\sigma \in S_n} (-1)^{\mathcal{E}_{R, S}(\sigma)},
		\label{eq:sylvesterPermanentExpansion}
	\end{equation}
	where we denoted with $R_{\sigma(k)}\odot S_k$ the \textit{bitwise dot product} between $R_{\sigma(k)}$ and $S_k$, defined as
	$R_{\sigma(k)} \odot S_k \equiv \bigoplus_{\alpha=1}^p R_{\sigma(k),\alpha} S_{k,\alpha}$,
	$C$ is a constant factor, 
	and we defined the expression at the exponent of the last expression
	(see \cref{lemma:sumModulo2AtExponent}) as
	\begin{equation}
		\mathcal{E}_{R, S}(\sigma)
		\equiv \bigoplus_{k=1}^n R_{\sigma(k)}\odot S_k
		= \bigoplus_{k=1}^n \bigoplus_{\alpha=1}^p R_{\sigma(k),\alpha} S_{k,\alpha}.
		\label{eq:sylvesterPermanentExponent}
	\end{equation}
	In the following, we will omit for brevity the dependence of the exponent $\mathcal{E}$ from the states and write the above expression as simply $\mathcal E(\sigma)$.
	Note that the actual value of $\mathcal{E}(\sigma)$ is not important here:
	the only relevant detail is its parity.
%	This is because the only two values that each summand can assume are either $+1$ or $-1$,
%	occurring if $\mathcal E(\sigma)$ is even or odd, respectively.
%	Because of this we could have equivalently defined $\mathcal E$
%	in \eqref{eq:sylvesterPermanentExponent} with sums modulo 2 instead of regular sums.
	
	Since we need to evaluate only the cancellation of the scattering amplitude $\mathcal A$, we will ignore the constant factor $C$ and focus on the sum over the permutations in \cref{eq:sylvesterPermanentExpansion}.
	For this sum to vanish, it is \textit{necessary and sufficient} that for exactly half of the permutations we have $(-1)^{\mathcal E(\sigma)}=1$.
	In other words,	the scattering amplitude $\mathcal A$ vanishes if and only if the number of permutations $\sigma$ such that $\mathcal E(\sigma)$ is even, is equal to the number of permutations $\sigma'$ such that $\mathcal E(\sigma')$ is odd.
	A necessary and sufficient condition for this to hold,
	is that to each permutation $\sigma$ we can univocally associate another different
	permutation $\sigma'$ such that
	$\mathcal E(\sigma') = 1 \oplus \mathcal E(\sigma)$.

	But if \cref{eq:suppressionConditionForR} holds,
	then by \cref{lemma:implicationsOfNeutralNegation} and \cref{lemma:partitionOfSetOfPermutations} we can univocally associate to each $\sigma$ the (different) permutation
	$\sigma_\tau \equiv \tau \circ \sigma$, where $\tau$ is a permutation such that
	$\mathcal N^A(R)=R_\tau$.
	Using $\sigma_\tau$ in \cref{eq:sylvesterPermanentExponent} we have
	\begin{equation}
	  \mathcal E(\sigma_\tau) =
	  \bigoplus_{k=1}^n \bigoplus_{\alpha=1}^p
	  R_{\tau(\sigma(k)),\alpha} S_{k,\alpha} =
	  \bigoplus_{k=1}^n \left[
		\left( \bigoplus_{\alpha \in A} R_{\tau(\sigma(k)),\alpha} S_{k,\alpha} \right)
		\oplus
		\left( \bigoplus_{\alpha \notin A} R_{\tau(\sigma(k)),\alpha} S_{k,\alpha} \right)
	  \right].
	  \label{eq:sylvesterEsigmatau}
	\end{equation}
	Using now the explicit expression for $\mathcal N^A(R)$ given in \cref{def:negationOfColumns}
	and the last result of \cref{lemma:implicationsOfNeutralNegation}, we have
	\begin{equation}
		\mathcal N^A(R_\sigma)=R_{\tau\circ\sigma}
		\Longleftrightarrow
		\begin{cases}
			1\oplus R_{\sigma(k),\alpha} = R_{\tau(\sigma(k)),\alpha}, & \alpha \in A,\\
			R_{\sigma(k),\alpha} = R_{\tau(\sigma(k)),\alpha}, & \alpha \notin A.
		\end{cases}
	\end{equation}
	Inserting these equations in \eqref{eq:sylvesterEsigmatau}, we finally obtain
	\begin{equation}
		\mathcal E(\sigma_\tau) =
		\mathcal E(\sigma) \oplus
		\left[ \bigoplus_{k=1}^n \bigoplus_{\alpha \in A} S_{k,\alpha} \right]
		= \mathcal E(\sigma) \oplus 1,
	\end{equation}
	where in the last step we exploited \cref{eq:suppressionConditionForS} for $S$.
	Inserting this last result into \cref{eq:sylvesterPermanentExpansion} we conclude that
	\begin{equation}
		\mathcal A = C \sum_{\sigma \in S_n} (-1)^{\mathcal E_{R,S}(\sigma)}
		= C \,\,\, \sum_{\mathclap{\sigma \in S_n : \, \mathcal E(\sigma) \text{ even}}} \,\,\,
		\left[ (-1)^{\mathcal E(\sigma)} + (-1)^{\mathcal E(\sigma_\tau)} \right]
		= 0,
	\end{equation}
	which proves that the input/output pair $(\mathbf r, \mathbf s)$ is suppressed.
\end{proof}

\begin{example}
	Let $r$ be the MOL given in \cref{eg:11110000}.
	Then $\mathcal N^{A}(\textbf r)=\textbf r$ holds for $A=\{2\}, A=\{3\}$. and $A=\{2,3\}$.
	\Cref{thm:mySuppressionLaw} predicts that all output states $\sstate$, which \ac{BM} representation $\Sstate$ has an odd number of 1s in the second or third column, are suppressed.
	Moreover, all output states with \ac{BM} $\Sstate$ having a total number of 1s in the second \emph{and} third column, are suppressed.
	More explicitly, all output states $\textbf s$ such that
	\begin{equation*}
		\bigoplus_{k=1}^n S_{k,2} = 1
		\qquad \text{or} \qquad
		\bigoplus_{k=1}^n S_{k,3} = 1
		\qquad \text{or} \qquad
		\bigoplus_{k=1}^n \left[S_{k,2}\oplus S_{k,3}\right] = 1,
	\end{equation*}
	are predicted to be suppressed.
	For example, the states
	$\textbf S=(3,6,7,8)$, $\textbf S=(2,6,7,8)$, and $\textbf S=(4,6,7,8)$,
	having BM representations
	\begin{equation*}
		\begin{pmatrix} 0&1&0\\1&0&1\\1&1&0\\1&1&1 \end{pmatrix},
		\qquad
		\begin{pmatrix} 0&0&1\\1&0&1\\1&1&0\\1&1&1 \end{pmatrix},
		\qquad \text{and} \qquad
		\begin{pmatrix} 0&1&1\\1&0&1\\1&1&0\\1&1&1 \end{pmatrix},
	\end{equation*}
	respectively, are all suppressed.
\end{example}

\begin{remark}[Efficiency]
	To check if \cref{thm:mySuppressionLaw} applies to a given input-output pair, one has to verify condition \ref{eq:suppressionCondition} for each of the $2^p-1=m-1$ possible (non empty) subsets of the $p$ columns of $R$ and $S$.
	Moreover, the number of elementary operations necessary to verify condition \ref{eq:suppressionCondition} for a given set of columns increases polynomially in $n$.
	In conclusion, the amount of computational resources required to verify the hypotheses of \cref{thm:mySuppressionLaw} increases polynomially in $n$ and $m$, and the proposed suppression law is therefore efficiently verifiable.
	In \cref{fig:timingsLogVsSupp} is shown how the computational times scale polynomially in $n$ with the suppression law, as opposite to exponentially in $n$ with the brute-force method of computing the permanents.
\end{remark}

\begin{figure}[tbph!]
    \centering
	\includegraphics[width=1\textwidth]{timingsLogVsSupp_continuousLines.pdf}
	\caption{
		\textbf{(a)} Computational times required to assess whether a randomly sampled pair is suppressed, by using the brute-force method of calculating the permanent (in green) or by using the suppression law (blue), plotted in logarithmic scale as a function of the number of $n$ and $p=\log_2 m$.
		Continuous lines: linear fits of the data, showing how the computational time with the brute-force method scales exponentially in $n$, as opposite to the linear scaling in $n$ required by the suppression law.
		\textbf{(b)} Average number of seconds required to assess if a randomly sampled pair is suppressed using the suppression law.
		The linear fits show that the scaling is not only polynomial, but roughly linear in $n$ for each $m$ (we note that the interesting regime for boson sampling experiments is $n \ll m$, therefore the irregularities shown for $n \approx m$ are negligible).
	}
	\label{fig:timingsLogVsSupp}
\end{figure}
\FloatBarrier

While \cref{eq:suppressionConditionForS} gives a \textit{sufficient} condition for an input-output pair to be suppressed,
this condition is not \textit{necessary}.
For the majority of input states, not \textit{all} of the suppressed output states satisfy \cref{thm:mySuppressionLaw}.
For a more precise analysis of these quantities the following definitions will be useful:
\begin{definition}
	Let $U$ be an \dimSq{m} unitary matrix.
	Then we will denote with $\Omega(U)$ the set of input-output (collision-free) suppressed pairs, and with $\Omega_{\operatorname{eff}}(U)$ those predicted to be suppressed by \cref{thm:mySuppressionLaw}:
	\begin{equation}
		\Omega(U) \equiv \left\{ (\mathbf R, \mathbf S) \in Q_{n,m} \!\times\! Q_{n,m} \, | \,\, \operatorname{perm}(U[\Rstate | \Sstate]) = 0 \right\},
	\end{equation}
	\begin{equation}
		\Omega_{\operatorname{eff}}(U) \equiv \left\{ (\mathbf R, \mathbf S) \in \Omega(U) \, | \,\, \mathcal N^A(R)=R_\tau \text{ and } \oplus_{k,\alpha} S_{k,\alpha} = 1 \right\}.
	\end{equation}
	In other words, $\Omega(U)$ is the set of all \dimSq{n} submatrices of $U$ whose permanent vanishes, while $\Omega_{\operatorname{eff}}(U)$ is the subset of the pairs in $\Omega(U)$ for which we can \emph{efficiently predict} that the permanent vanishes.

	Clearly, we have $\Omega_{\operatorname{eff}}(U) \subset \Omega(U)$ for any $U$.
	\label{def:omegas}
\end{definition}

An interesting question regards the evaluation of the ratio $\left| \Omega_{\operatorname{eff}}(U^S) \right| / \left| \Omega(U^S) \right| $, i.e. the amount of suppressed pairs caught by \cref{thm:mySuppressionLaw}, which will be analyzed in some detail in the next section.

There is however at least one class of inputs for which \cref{thm:mySuppressionLaw} seems to completely characterize the output suppressed states (that is, the suppressed output states are all and only those predicted to be suppressed by \cref{thm:mySuppressionLaw}):

\begin{conjecture}
	Let \textbf{r} and \textbf{s} be two many-boson states of $n=2^q$ bosons in $m=2^p$ modes, let $R$ and $S$ be the corresponding BM representations, let $A$ be a subset of the columns of $R$, and let $\alpha(\mathbf r)$ be the number of subsets $A$ such that condition \ref{eq:suppressionConditionForR} holds, that is
	\begin{equation}
		\alpha(\mathbf r) \equiv 
		\left| \{ 
			A \subseteq \mathcal P(\{1,\dots,p\}) :
			\mathcal N^A(\mathbf{r}) = \mathbf{r}
		\} \right|,
	\end{equation}
	where we adopted the standard convention of denoting with $\mathcal P(X)$ the power set of a set $X$.
	If $\alpha(\mathbf r)=n-1$, then the output suppressed states are \textbf{all and only} those satisfying \cref{eq:suppressionConditionForS}.
	\label{thm:mySuppressionLawIFF}
\end{conjecture}

\begin{remark}
	\Cref{thm:mySuppressionLawIFF} predicts that for a (rather small) class of input states, the set of output suppressed states is \textit{completely characterized} by \cref{thm:mySuppressionLaw}, and has been verified by brute-force computations for $n=2,4,8,16$.
	We note that this class of states includes as a subset those considered by \cref{thm:sylvester_suppression_law}, for which the result has been proved in \cite{crespi2015suppression}.
\end{remark}

In the next section we will present the results of a series of numerical computations carried out to determine the dependence of the fraction of predicted suppressed pairs from $m$ and $n$.
%Unfortunately, \cref{thm:mySuppressionLaw} does not provide the total number of suppressed input/output pairs, nor it provides an easy way to estimate what fraction of the suppressed pairs can be predicted this way.
%Similarly, we don't yet have a way to analytically compute the ratio of suppressed pairs which obey \cref{thm:mySuppressionLaw}.
%As can be seen in figs {\bfseries\itshape\color{red} DA INSERIRE ANDAMENTI FRAZIONI DI SOPPRESSI CON SUPPRESSION LAW} this ratio does not seem to follow a trivial behaviour.


\section{Bayesian validation scheme}
\label{sec:ch5:BayesianValidationScheme}

To assess if a given validation protocol is scalable at increasing $n$, it is useful to quantify the number of experimental runs needed to reject an alternative hypothesis with a given confidence level.
Here we propose a simple model to predict the mean number of samples required to reject the hypothesis $\mathcal C$ of distinguishable photons, and accept an hypothesis $\mathcal{Q_{\epsilon}}$ of \textit{nearly} indistinguishable photons, being $\epsilon$ a parameter quantifying the amount of distinguishability introduced by experimental errors.

Let $p\equiv \left| \Omega_{\operatorname{eff}}(U^S) \right| / \binom{m}{n}^2$ be the fraction of suppressed $n$-photon input/output pairs predicted by \cref{thm:mySuppressionLaw}.
Given the unbiased nature of Sylvester matrices, $p$ is also the probability of observing an event in a suppressed pair, when sampling uniformly from the set of all input-output pairs.
The probability of observing an event in a suppressed pair in the $\mathcal Q_\epsilon$ hypothesis will be instead quantified by the parameter $\epsilon$, and assumed to be a small number $p_Q(\epsilon) = \epsilon p$ (the ideal quantum case will therefore correspond to $\epsilon =0$).

The only assumption of the proposed validation scheme is that {\itshape for some \bfseries unknown} value $\epsilon_0$, the probability of experimentally detecting an event in a suppressed pair is given by $p_{Q}(\epsilon_0) = \epsilon_0 p$.
We will then give a formula to assess if a given set of experimental data is more likely (with some fixed confidence level) to come from a $\mathcal Q_\epsilon$ evolution as opposite to the distinguishable case $\mathcal C$.
More in particular, the proposed validation scheme will accept a whole range of values of $\epsilon$ such that the relative probability of the data coming from $\mathcal Q_\epsilon$ as opposed to $\mathcal C$ will be greater then a confidence level $\alpha$.

Let $M$ denote the total number of samples collected in an experiment, and let $M_{\operatorname{supp}}$ be the number of those samples which turned out to be a suppressed pair according to \cref{thm:mySuppressionLaw}.
Then, assuming unbiased prior probabilities, the relative probability $\mathcal V_\epsilon$ of the hypothesis $\mathcal Q_\epsilon$ over the hypothesis $\mathcal C$, having observed $M_{\text{supp}}$ efficiently predicted suppressed pairs over $M$ samples, is
\begin{equation}
	\mathcal{V}_\epsilon \equiv \frac{
		P( \mathcal Q_\epsilon \,\, | \,\, M,M_{\text{supp}})
	}{
		P( \mathcal C \,\, | \,\, M,M_{\text{supp}})
	}
	= \frac{
		P( M,M_{\text{supp}} | \,\, \mathcal Q_\epsilon)
	}{
		P( M,M_{\text{supp}} | \,\, \mathcal C)
	}.
	\label{eq:relativeProbability}
\end{equation}
In the case of completely distinguishable photons the probability is
\begin{equation}
	P( M,M_{\text{supp}} | \,\, \mathcal C) = p^{M_{\text{supp}}}(1-p)^{M-M_{\text{supp}}}
	\label{eq:probability:C}
\end{equation}
while if the dynamic of the system follows $\mathcal Q_{\epsilon}$ the probability is
\begin{equation}
	P( M,M_{\text{supp}} | \,\, \mathcal{Q_\epsilon}) = (\epsilon p)^{M_{\text{supp}}} (1-\epsilon p)^{M-M_{\text{supp}}}.
	\label{eq:probability:Q}
\end{equation}
Using \cref{eq:probability:C,eq:probability:Q} inside \cref{eq:relativeProbability} gives
\begin{equation}
	\mathcal V_\epsilon = \epsilon^{M_{\text{supp}}} \frac{
		(1-\epsilon p)^{M-M_{\text{supp}}}
	}{
		(1-p)^{M-M_{\text{supp}}}
	},
	\label{eq:relativeProbability2}
\end{equation}
and we will consider $\mathcal Q_\epsilon$ to be successfully distinguished from $\mathcal C$, with the confidence level $\alpha$, for all the values of $\epsilon$ such that $\mathcal V_\epsilon \ge \alpha$.

Let's now denote with $\epsilon_0$ the parameter quantifying the \textit{real} experimental distinguishability of the experiment (which is assumed to be unknown for the test).
Then, on average, the number of detected suppressed pairs will be $M_{\text{supp}} = \epsilon_0 p M$.
Plugging this into \cref{eq:relativeProbability2} we obtain
\begin{equation}
	\mathcal V_\epsilon =
	\left[
		\epsilon^{\epsilon_0 p}
		\left( \frac{1-\epsilon p}{1-p} \right)^{1-\epsilon_0 p}
	\right]^M.
	\label{eq:relativeProbability3}
\end{equation}
In \cref{fig:validationRangeOfEpsilon} is shown an example of application of this test, using \cref{eq:relativeProbability3}.
For a fixed set of values for $M, \alpha, \epsilon_0$, and $p$, the \emph{accepted} models $\mathcal Q_\epsilon$ are those corresponding to the range of values of $\epsilon$ such that $\mathcal V_\epsilon \ge \alpha$.
In \cref{fig:validationRangeOfEpsilon} this is equivalently expressed by plotting $(\mathcal V_\epsilon)^{1/M}$ (blue line) against $\epsilon$, and comparing it with the value of $\alpha^{1/M}$ (purple line).
All values of $\epsilon$ for which the blue line is above the purple one are accepted, and thus distinguished from the classical distribution with confidence $\alpha$.
Note how the accepted values of $\epsilon$ are those close to $\epsilon_0$ (red dotted line), which is the parameter characerizing the actual distinguishability of the experimental apparatus.

% \begin{figure}[tb]
% 	\centering
% 	\includegraphics[width=.6\textwidth]{validationRangeOfEpsilon.pdf}
% 	\caption{
% 		Accepted range of values of $\epsilon$, for $M=300$, $\alpha=2000$, $\epsilon_0=0.3$, $p=0.1$.
% 		The blue line is a fit of $(\mathcal V_\epsilon)^{1/M}$.
% 		The solid green line corresponds to $\mathcal V_\epsilon=1$, the dotted red line to $\epsilon=\epsilon_0$, and the dotted purple line to $(\mathcal V_\epsilon)^{1/M} = \alpha^{1/M}$.
% 		All the values of $\epsilon$ corresponding to the blue line being above the purple one are accepted by the test, with the confidence $\alpha$.
% 		This range of accepted values is highlighted in the figure by the solid purple line.
% 	}
% 	\label{fig:validationRangeOfEpsilon}
% \end{figure}
\begin{figure}[tbh!]
    \floatbox[{
        \capbeside\thisfloatsetup{capbesideposition={right,center},capbesidewidth=7cm}
    }]{figure}[\FBwidth]
    {
    	\caption{
        	Accepted range of values of $\epsilon$, for $M=300$, $\alpha=2000$, $\epsilon_0=0.3$, $p=0.1$.
        	The blue line is a fit of $(\mathcal V_\epsilon)^{1/M}$.
        	The solid green line corresponds to $\mathcal V_\epsilon=1$, the dotted red line to $\epsilon=\epsilon_0$, and the dotted purple line to $(\mathcal V_\epsilon)^{1/M} = \alpha^{1/M}$.
        	All the values of $\epsilon$ corresponding to the blue line being above the purple one are accepted by the test, with the confidence $\alpha$.
        	This range of accepted values is highlighted in the figure by the solid purple line.
		}
        \label{fig:validationRangeOfEpsilon}
    }
    {\includegraphics[width=8cm]{validationRangeOfEpsilon.pdf}}
\end{figure}

Inverting \cref{eq:relativeProbability3} we can extract the average number $M_{\epsilon,\epsilon_0}$ of experimental runs needed to reject $\mathcal C$ and accept $\mathcal Q_\epsilon$ with confidence $\alpha$ is
\begin{equation}
	M_{\epsilon,\epsilon_0} = \frac{ \log \alpha}{
		\epsilon_0 p \log \epsilon + (1-\epsilon_0 p) \log{\left(\frac{1-\epsilon p}{1-p}\right)}
	}.
	\label{eq:neededNumberOfSamples}
\end{equation}

As a function of $\epsilon$, $M_{\epsilon,\epsilon_0}$ has a positive minimum for $\epsilon=\epsilon_0$, as can be seen for example in \cref{fig:bayesianRelativeProbabilityEpsilon}.
Setting thus $\epsilon=\epsilon_0$ in \cref{eq:neededNumberOfSamples} we obtain that \textit{the average number of experimental samples required to assess with confidence $\alpha$ that the observed data are not compatible with the evolution of distinguishable photons, is}
\begin{equation}
	M_{\epsilon_0,\epsilon_0} = \frac{ \log \alpha}{
		\epsilon_0 p \log \epsilon_0 + (1-\epsilon_0 p) \log{\left(\frac{1-\epsilon_0 p}{1-p}\right)}
	}.
	\label{eq:neededSamplesMinimum}
\end{equation}
A rough approximation of \cref{eq:neededSamplesMinimum} for small $\epsilon_0$ is
\begin{equation}
	M_{\epsilon_0,\epsilon_0} \approx \frac{\log \alpha}{\log{\left( \frac{1}{1-p} \right)} } + \mathcal O(\epsilon_0),
	\label{eq:neededSamplesMinimumApprox}
\end{equation}
which for small $p$ gives the scaling behaviour $M_{\epsilon_0,\epsilon_0} \sim \log \alpha /p$.
We therefore conclude that the number of experimental runs needed to validate against distinguishable photons scales polynomially in $n$ if and only if the condition 
% $p > 1/\operatorname{poly}(n)$
\begin{equation}
	p > 1/\operatorname{poly}(n)
	\label{eq:efficientValidationCondition}
\end{equation}
is satisfied, for small values of $\epsilon_0$ and $p$.

% \begin{figure}[tb]
%     \centering
% 	\includegraphics[width=0.6\textwidth]{bayesianRelativeProbability.pdf}
% 	\caption{
		% Needed number of samples $M_{\epsilon,\epsilon_0}$(\cref{eq:neededNumberOfSamples}) as a function of $\epsilon$, for $\epsilon_0=0.3$, $p=0.1$, and $\alpha=2000$.
		% The range of values of $\epsilon$ corresponding to $\mathcal V_\epsilon < 1$ result in $M_{\epsilon,\epsilon_0}<0$.
		% For these $\epsilon$ the data is more likely to come from $\mathcal C$ than $\mathcal Q_\epsilon$.
		% The green dashed line highlights the value of $\epsilon=\epsilon_0$.
% 	}
% 	\label{fig:bayesianRelativeProbabilityEpsilon}
% \end{figure}
\begin{figure}[tbh!]
    \floatbox[{
        \capbeside\thisfloatsetup{capbesideposition={right,center},capbesidewidth=7cm}
    }]{figure}[\FBwidth]
    {
    	\caption{
        	Needed number of samples $M_{\epsilon,\epsilon_0}$(\cref{eq:neededNumberOfSamples}) as a function of $\epsilon$, for $\epsilon_0=0.3$, $p=0.1$, and $\alpha=2000$.
        	The range of values of $\epsilon$ corresponding to $\mathcal V_\epsilon < 1$ result in $M_{\epsilon,\epsilon_0}<0$.
        	For these $\epsilon$ the data is more likely to come from $\mathcal C$ than $\mathcal Q_\epsilon$.
        	The green dashed line highlights the value of $\epsilon=\epsilon_0$.
		}
        \label{fig:bayesianRelativeProbabilityEpsilon}
    }
    {\includegraphics[width=8cm]{bayesianRelativeProbability.pdf}}
\end{figure}

\FloatBarrier
\section{Conclusions}
\label{sec:ch5:conclusions}

Further work is needed to assess whether the fraction of suppressed pairs given by Sylvester matrices  satisfies \cref{eq:efficientValidationCondition}.
As shown in \cref{table:suppressionFractions,fig:scattershotSamplingSuppressionRatios}, the number $\Omega(U)$ of suppressed pairs is significantly larger for Sylvester matrices than for Fourier matrices.
While it is clear that the former is better suited for scattershot sampling validation than the latter, the scaling behaviour of $\Omega(U^S)$ can hardly be deduced from the data in \cref{table:suppressionFractions,fig:scattershotSamplingSuppressionRatios}-a.

\begin{table}[h!]
	\centering
	\begin{tabular}{ |c|c|c|c| } 
		\hline 
		m & n & Sylvester & Fourier \\[2px]
		\hline
		8 & 2 & 4/7 $\simeq$ 57.14\%  & 12/49 $\simeq$ 24.49\% \\ 
		\hline
		8 & 3 & 0 & 8/49 $\simeq$ 16.33\% \\
		\hline
		8 & 4 & 96/175 $\simeq$ 54.86\% & 52/245 $\simeq$ 21.22\% \\
		\hline
		8 & 5 & 4/7 $\simeq$ 57.14\% & 12/49 $\simeq$ 24.49\% \\
		\hline
		8 & 6 & 4/7 $\simeq$ 57.14\%  & 24/49 $\simeq$ 48.98\% \\ 
		\hline
		8 & 7 & 0 & 0 \\
		\hline
		16 & 2 & 8/15 $\simeq$ 53.33\% & 32/225 $\simeq$ 14.22\% \\ 
		\hline
		16 & 3 & 0 & 64/1225 $\simeq$ 5.22449\% \\
		\hline
		16 & 4 & 480/1183 $\simeq$ 40.57\% & 11112/207025 $\simeq$ 5.37\% \\
		\hline
		16 & 5 & 480/1183 $\simeq$ 40.57\% & 632/24843 $\simeq$ 2.54\% \\
		\hline
		16 & 6 & 53160/143143 $\simeq$ 37.14\% & 256/11011 $\simeq$ 2.32\% \\
		% \hline
		% 16 & 7 & 0 & {\color{red}yet to compute} \\
		\hline
		16 & 8 & 21952/83655 $\simeq$ 26.24\% & 495172/41409225 $\simeq$ 1.20\% \\
		\hline
	\end{tabular}
	\caption{Fractions of suppressed pairs (i.e. $\Omega(U)/\binom{m}{n}^2$) for Sylvester and Fourier matrices, and various values of the number of modes $m$ and the number of photons $n$. The values are calculated computing, with the permanent formula, all the scattering amplitudes between collision-free input-output states.}
	\label{table:suppressionFractions}
\end{table}

\FloatBarrier
\clearpage
\begin{figure}[tb]
    \centering
	\includegraphics[width=1\textwidth]{scattershotSamplingSuppressionRatios_withLetters.pdf}
	\caption{
		\textbf{Estimated fraction of suppressed pairs with Fourier and Sylvester matrices.}
		\textbf{(a)}
		Fraction of suppressed pairs with Sylvester matrices, that is, $\Omega(U^S)/\binom{m}{n}^2$, plotted against $n$ for various values of $m$.
		The data is obtained randomly sampling $N=1000$ input-output pairs, and computing the permanent corresponding to each scattering amplitude.
		The percentage is known to be zero for number of photons of the form $n=2^p-1$ \cite{simion1983+}, regardless of $m$.
		\textbf{(b)}
		Fraction of suppressed pairs with Fourier matrices, that is, $\Omega(U^{\operatorname{F}})/\binom{m}{n}^2$, plotted against $n$ for various values of $m$.
		The fraction is in this case significantly lower than for Sylvester matrices.
		For most values of $n$, no suppressed pair was found in the sampled dataset.
	}
	\label{fig:scattershotSamplingSuppressionRatios}
\end{figure}
\FloatBarrier

In \cref{fig:scattershotSamplingSuppressionRatios_fourierAndSylvester} are again given the estimated fractions for Fourier and Sylvester matrices.
The error bars in \cref{fig:scattershotSamplingSuppressionRatios_fourierAndSylvester} represent the 99.7 \% confidence interval, calculated through Bayesian inference from the number of suppressed pairs found in the sampled datasets.
In particular, in \cref{fig:scattershotSamplingSuppressionRatios_fourierAndSylvester}-a the fraction $\Omega(U^{\operatorname{S}})$ depends weakly on $m$, as opposite to the strong dependence of $U^{\operatorname{F}}$ on $m$ shown in \cref{fig:scattershotSamplingSuppressionRatios_fourierAndSylvester}-b.

Another relevant matter is the scaling with $m$ and $n$ of $\Omega_{\operatorname{eff}}(U^{\operatorname{S}})$, that is, the number of suppressed pairs satisfying the conditions of \cref{thm:mySuppressionLaw}.
No efficiently evaluable analytical expression is known for this number.
Brute-force estimates are given in \cref{fig:sss_sylvester_predicted}, where calculations similar to those in \cref{fig:scattershotSamplingSuppressionRatios_fourierAndSylvester} are shown, but this time for the \emph{efficiently predictable} suppressed pairs.
In the presented cases of $m=16,32,64,128$, $\Omega_{\operatorname{eff}}(U^{\operatorname{S}})$ seems to decrease exponentially in $n$, for $n < m/2$.

In conclusion, while \cref{thm:mySuppressionLaw} represents a significant improvement with respect to \cref{thm:tichy_suppression_law} and \cref{thm:sylvester_suppression_law}, this fraction is probably still not large enough for an efficient validation test with Sylvester matrices.
However the data on the \emph{total} fraction of suppressed states presented in \cref{fig:scattershotSamplingSuppressionRatios,fig:scattershotSamplingSuppressionRatios_fourierAndSylvester} leaves open the possibility that, further improving on \cref{thm:mySuppressionLaw}, an efficient validation scheme for Sylvester matrices may be devised.

\begin{figure}[tbph!]
	\includegraphics[width=.9\textwidth]{sss_fourier_sylvester_withLetters.pdf}
	\caption{
        \textbf{Fraction of suppressed pairs with Fourier and Sylvester matrices.}
        Each color represents the fraction corresponding to a different number of modes $m$, plotted as a function of the number of photons $n$.
        The error bars approximately represent the 99.7\% confidence interval, estimated from the sampled data through Bayesian inference.
        The approximation used breaks for very small number of found suppressions, which is seen in the points where the error bars span negative values.
	}
	\label{fig:scattershotSamplingSuppressionRatios_fourierAndSylvester}
\end{figure}

\begin{figure}[tbph!]
    \centering
	\includegraphics[width=.9\textwidth]{sss_sylvester_predicted.pdf}
	\caption{
		\textbf{Estimated fraction of \textit{predicted} suppressed pairs with Sylvester matrices.}
		Plot of the percentage of \emph{predicted} suppressed input-output pairs against $n$, for various values of $m$, for Sylvester matrices.
        The data is calculated randomly sampling input-output collision-free pairs, and verifying whether each is suppressed according to \cref{thm:mySuppressionLaw}.
        The missing data corresponds to values of $n$ at which no suppression in the sampled pairs was found.
        For $m=64$, the size of the sample for which no suppressions were found is $N= 16 \times 10^3$, while for $m=128$ is $N = 2000$.
        Larger samples were not used due to the heavy computational times required.	
	}
    \label{fig:sss_sylvester_predicted}
\end{figure}