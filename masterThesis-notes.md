# History of classical information

- The modern incarnation of computer science was announced by Alan Turing in a **1936** paper, where he developed in detail an abstract notion of what we would now call a *programmable computer*. He also showed that there is *Universal Turing Machine* that can be used to simulate any other Turing machine, and claimed that <font color="green">the Universal Turing Machine completely captures the notion of what it means to perform a task by algorithmic means</font>. This assertion is now known as the **Church-Turing thesis**.
- **1947**: Hardware took off thanks Bardeen, Brattain and Shockley.
- **1965**: Moore's law
- In the **late 1960s and early 1970s** it seemed as though the Turing model of computation was as powerful as any other model of computation. This observation of codified into a strengthened version of the Chuch-Turing thesis: the so-called **strong Church-Turing thesis** states that *any algorithmic process can be simulated efficiently	using a Turing machine*.
- The first major challenge to the strong Church-Turing thesis came in the mid **1970s**, when **Robert Solovay and Volker Strassen** devised an efficient *probabilistic* primality test that used randomness as an essential part of the algorithm. Given that no efficient deterministic primality test was known, and nor it is now, the Solovay-Strassen algorithm hinted to modify the Church-Turing thesis in a probabilitic form: *any algorithim process can be simulated efficiently using a probabilistic Turing machine*.
- In **1985** **David Deutsch** investigated the idea of a *universal model of computation*, that would be as secure as the status of that physical theory. At this time, it is not clear whether Deutsch's notion of a Universal Quantum Computer is sufficient to efficiently simulate an arbitrary physical system. It is possible, for example, that some effect of quantum field theory or an even more esoteric effect bsaed in string theory, quantum gravity or some other physical theory may take us beyond Deutsch's Universal Quantum Computer, giving us a still more powerful model for computation. What Deutsch's model of quantum computer did enable was a challenge to the strong form of the Church-Turing thesis. He also constructed a simple example suggesting that, indeed, quantum computers might have computational powers exceeding those of classical computers.
- In **1994** **Peter Shor** proved that two important problems - the problem of finding the prime factors of an integer, and the so-called *discrete logarithm* problem - could be solved efficiently on a quantum computer.
- Further evidence came in **1995** when Lov Grover showed that another import problem - the problem of conducting a search through some unstructured search space - could also be sped up on a quantum computer.

# Complexity classes
##Decision problems
- Many computational problems are most cleanly formulated as *decision problems*, that is problems with a yes or no answer. For example, the question *is a given number m a prime number or not?* is a decision problem.
- Although most decision problems can easily be stated in simple, familiar language, discussion of the general properties of decision problems is greatly helperd by the terminology of *formal languages*. In this terminology, a *language L* over the *alphabet* &Sigma; is a subset of the set &Sigma;<sup>*</sup> of all finite strings of symbols from &Sigma;. For example, if &Sigma;={0,1}, then the set of binary representations of even numbers, L={0,10,100,110,...} is a language over &Sigma;.
- Decision problems may be encoded naturally as problems about languages. For instance, the primality decision problem can be encoded using the binary alphabet &Sigma; = {0,1}, interpreting strings from &Sigma;<sup>\*</sup> as non-negative integers, and defining the language L to consist of all binary strings such that the corresponding number is prime. The primality decision problem is then translated in the problem of finding a TM which, when started with a given number n on its input tape, eventually outputs some equivalent of *yes* if n is prime, and outputs an equivalent of *no* if n is not prime.
## Functions problems
- A function problem is a computational problem where a single output is expected for every input, but the output is more complex than a simple YES or NO like for decision problems.
- asd

# Linear optics quantum computation
- All these systems have their own advantages in quantum information processing. However, even though there may now be a few front-runners, such as ion-trap and superconducting quantum computing, **no physical implementation seems to have a clear edge over others at this point**. This is an indication that the technology is still in its infancy.
- Optical quantum systems are prominent candidates for quantum computing, since they provide a natural integration of quantum computation and quantum communication. (che cazzo vuol dire??)
- <font color="green">Linear quantum optics with single photons has the advantage that photons have long decoherence times, which means that the quantum information stored in photons tends to stay there. Also, linear optical elements may be the simplest building block to realize quantum information processing.</font> <font color="red">The downside is that photons hardly interact with each other, and in order to apply two-qubit quantum gates such interactions are essential. Because of this, to construct an optical quantum computers, effective interactions among photons have to be introduced somehow.</font>
- To introduce effective interactions among photons there are two ways: nonlinear Kerr media, and the use of projective measurements with photodetectors. The first one is still out of range for present-day technology due to the very low efficiency and nonlinearities of present-day nonlinear Kerr media. The second one suffers from the problem that the quantum gates obtained through projective measurements are nondeterministic. This potentially makes the implementation of reliable quantum gates nonscalable due to an exponentially increasing number of required optical elements.
- The KLM scheme introduced in 2001 achieves scalable LOQC using quantum gate teleportation	and error correction, requiring an amount of linear optical elements increasing only polynomially with the complexity of the computation.
- The downside of the KLM scheme is that, though allowing scalable LOQC, it still requires to be implemented to overcome a series of experimental challenges, such as the synchronization of pulses, mode-matching, quickly controllable delay lines, tunable beamsplitters and phase-shifters, single-photon sources, and accurate, fast, single-photon detectors. While most of these feats are not terribly unrealistic to implement, the experimental state of the art is simply not at the point at which more complex gate operations such as two-qubit operations can be implemented.

# Other references

## HOM
- Hong, Ou, Mandel - *Measurement of Subpicosecond Time Intervals between Two Photons by Interference* - 1987

## BECs
- Davis et al. - *Bose-Einstein Condensation in a Gas of Sodium Atoms* - 1995
- Anderson et al. - *Observation of Bose-Einstein Condensation in a Dilute Atomic Vapor* - 1995
- Klaers et al. - *Bose–Einstein condensation of photons in an optical microcavity* - 2010

# BosonSampling References
- Aaronson and Arkipov - *The Computational Complexity of Linear Optics* - 2010

## Reviews
- Gard et al. - *An introduction to boson-sampling* - 2014

## Hardness results
- Aaronson and Arkipov - *The Computational Complexity of Linear Optics* - 2010
- Rohde et al. - *Evidence for the conjecture that sampling generalized cat states with linear optics is hard* - 2015

## BosonSampling and ECT
- Shchesnovich - *Sufficient condition for the mode mismatch of single photons for scalability of the boson-sampling computer* - 2014
- Shchesnovich - *Conditions on the experimental Boson-Sampling computer to disprove the Extended Church-Turing thesis* - 2014
- Rohde et al. - *Will boson-sampling ever disprove the Extended Church-Turing thesis?* - 2014



## BosonSampling losses analyses
- Shchesnovich - *Sufficient condition for the mode mismatch of single photons for scalability of the boson-sampling computer* - 2014
- Rohde, Ralph - *Error tolerance of the boson-sampling model for linear optics quantum computing* - 2012
- Leverrier, Patron - *ANALYSIS OF CIRCUIT IMPERFECTIONS IN BOSONSAMPLING* - 2013,

## Bosonic bunching
- Peruzzo et al. - *Quantum Walks of Correlated Photons* - 2010
- Spagnolo et al. - *General Rules for Bosonic Bunching in Multimode Interferometers* - 2013


## Scattershot
- Lund et al. - *Boson Sampling from Gaussian States* - 2013
- Bentivegna et al. - *Experimental scattershot boson sampling* - 2015

## Experiments
- Bentivegna et al. - *Experimental scattershot boson sampling* - 2015