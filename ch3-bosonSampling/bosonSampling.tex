%!TEX root = ..\thesis.tex

In this chapter we discuss various aspects of the \BosonSampling{} computational problem.
In \cref{sec:importanceOfBosonSampling} the problem of experimentally assessing \emph{quantum supremacy} is discussed, in order to appreciate the importance of \BosonSampling{} in the modern research context.
\Cref{sec:BosonSamplingComputationalProblem} follows with the description of what the \BosonSampling{} computational problem is, and its advantages in obtaining experimental evidences of quantum supremacy.
In \cref{sec:scalingExperimentalBosonSamplingImplementations} some issues related to the scalability of boson sampling implementations are described.
The chapter closes with a description of \emph{scattershot boson sampling} in \cref{sec:scattershotBosonSampling}, as an alternative architecture to scale boson sampling implementations to higher numbers of photons.

\section{Importance of \BosonSampling{}}
\label{sec:importanceOfBosonSampling}

It is currently believed that many quantum mechanical systems cannot be efficiently simulated with a classical computer \autocite{feynman1982simulating}.
This implies that a quantum device is, to the best of our knowledge, able to solve problems \textit{de facto} beyond the capabilities of classical computers.
Exploiting this \textit{quantum advantage} requires however an high degree of control over the quantum system, not yet manageable with state of the art technology.
In particular, a post-classical computation with a universal quantum computer will require an high degree of control of a large number of qubits, and this implies that an experimental evidence of \textit{quantum supremacy} \autocite{preskill2012blog} with an universal quantum computer will likely require many years.
A notable example is given by the large gap between the number of qubits that can currently be coherently controlled ($\sim\! 10$), and the number of qubits required for a calculation such as prime factorization, on a scale that would challenge classical computers ($\sim\! 10^6$).
Consequently, there is considerable interest in non-universal quantum computers and quantum simulators that, while able to only solve specific problems, might be significantly easier to be implemented experimentally.
Such devices could give the first experimental demonstration of the power of quantum devices over classical computers, and potentially lead to technologically significant applications.

Moreover, in the context of searching for experimental evidence of quantum supremacy, the technological difficulties are not the only issue.
To show this, we will consider as an example Shor's quantum algorithm \cite{shor1997polynomial} to efficiently factorize integer numbers.
Even if we were to get past the technological difficulties of implementing Shor's algorithm with sufficiently many qubits, it could be easily argued that such an achievement would not be a conclusive evidence that quantum mechanics allows post-classical computations.
This because \textit{we do not have to date a mathematically sound proof that there cannot be a classical algorithm to efficiently factorize integers}.
In the language of complexity theory, this corresponds to the fact that we do not have a proof that \textsc{Factoring} is not in \textbf P, even though this is believed enough to base modern cryptography is based on this conjecture.
More generally, before 2010, there were no instances of problems efficiently solved by quantum computers, which were \textit{proved} to not be efficiently solvable with classical ones.

This changed when, in 2010, Aaronson and Arkhipov (AA) proposed \cite{aaronson2011computational} the \textsc{BosonSampling} problem as a way to obtain an easier experimental evidence of quantum supremacy.
\textsc{BosonSampling} is a \emph{computational problem} that, while hard to solve for a classical computer, is efficiently solved by a special-purpose quantum device.
AA showed that \BosonSampling{} is naturally implemented using only linear optical elements, in a photonic platform named a \emph{boson sampler}.
The experimental realization of a boson sampler, while still challenging with present-day technologies, requires much less experimental efforts with respect to those required to build a universal quantum computer.
In fact, the AA scheme requires only linear optical elements and photon-counting detectors, as opposite to, for example, the Knill, Laflamme $\&$ Milburn approach \cite{knill2001scheme,kok2007linear} for universal linear optics quantum computing, which requires among other things an extremely fast feedback control of the detectors.
% {\color{blue}
% This led to a series of experimental implementations of increasing complexity \cite{broome2013photonic,crespi2013integrated,tillmann2013experimental,spring2013boson,spagnolo2013three,spagnolo2014experimental,carolan2014experimental,bentivegna2015experimental,carolan2015universal,crespi2015quantum}.
% }

\begin{figure}[tbh!]
	\centering
	\includegraphics[width=\linewidth]{Galton.png}
	\caption{
		\textbf{Galton board:}
		$n$ identical balls are dropped one by one from the upper corner, and are randomly scattered into the lower slots.
		The quantum generalization of this classical ``rudimentary computer'' leads to the idea of \BosonSampling{}.
		Credits: Nicolò Spagnolo.
	}
	\label{fig:Galton_board}
\end{figure}

\FloatBarrier
\section{The \textsc{BosonSampling} computational problem}
\label{sec:BosonSamplingComputationalProblem}

Consider the following linear optical experiment: the $n$-photon state $\ket{\textbf r_{\operatorname{AA}}}$ given by
\begin{equation}
	%\ket{\psi_{\operatorname{in}}}
	\ket{\textbf{r}_{\operatorname{AA}}}
	\equiv \left| 1_1,\dots,1_n,0_{n+1},\dots,0_m \right\rangle
	\equiv \hat a_1^\dagger \dots \hat a_n^\dagger \left| 0_1,\dots,0_m \right\rangle,
	\label{eq:AAInputState}
\end{equation}
is injected into a passive linear optics network, which implements a unitary map on the creation operators:
\begin{equation}
	\adagger_k \rightarrow \hat U \bdagger_k \hat U^\dagger = \sum_{j=1}^m U_{k,j} \bdagger_j.
	\label{eq:creationOperatorEvolution}
\end{equation}
with $U$ an Haar-random \dimens{m}{m} complex unitary matrix.
The evolution induced on $\ket{\rstate_{AA}}$ is
\begin{equation}
	\ket{\rstate_{AA}} \rightarrow
	\sum_{\sstate \in F_{n,m}} \mathcal A(\rstate \rightarrow \sstate, U) \ket{\sstate},
\end{equation}
where the sum is extended over all many-boson states of $n$ particles into $m$ modes, and the scattering amplitudes $\mathcal A$ are, as shown in \cref{eq:scatteringAmplitudeWithPermanent2}, proportional to the permanents of \dimens{n}{n} matrices.
AA argued that, for $m \gg n$, the output of such an apparatus cannot be efficiently predicted by a classical computer, neither exactly nor approximately \cite{aaronson2011computational}.
This was rigorously proved in the exact case.
The problem of \emph{approximately} sampling from the output probability distribution of a boson sampling apparatus depends instead on a series of conjectures, for which strong supporting evidence was provided \cite{aaronson2011computational}.

This problem, which amounts to that of being able to \emph{sample} from the output probability distribution given in \cref{eq:scatteringAmplitudeWithPermanent2}, is referred to as the \emph{\textsc{BosonSampling} computational problem}.
The constraint $m \gg n$ is essential for the hardness result, as otherwise semi-classical methods become efficient \cite{tichy2011entanglement,shchesnovich2013asymptotic}. 

Roughly speaking, a boson sampling apparatus is a ``quantum version'' of a \emph{Galton board}.
A Galton board, named after the English scientist Sir Francis Galton, is an upright board with evenly spaced pegs into its upper half, and a number of evenly-spaced rectangular slots in the lower half (see \cref{fig:Galton_board}).
This setup can be imagined to be a rudimentary ``computer'', where $n$ identical balls are dropped one by one from the upper corner, and are randomly scattered into the lower slots.
In the quantum mechanical version, the $n$ balls are indistinguishable bosons ``dropped'' simultaneously, and each peg a unitary transformation, typically implemented as a set of beam splitters and phase shifters.

More precisely, \textsc{BosonSampling} consists in producing a fair sample of the output probability distribution $P(\sstate \, | U, \textbf{r}_{\operatorname{AA}}) \equiv |\mathcal A(\rstate \rightarrow \sstate, U) |^2$, where $\textbf{s}$ is an output state of the $n$ bosons, and $\textbf{r}_{AA}$ the above mentioned input state.
The unitary $\hat U$ and the input state $\rstate_{\text{AA}}$ are the \emph{input} of the \BosonSampling{} problem, while a number of output states sampled from the correct probability distribution are its \emph{solution} (see \cref{fig:bosonSampling_scheme}).

\begin{figure}[tbh!]
	\centering
	\includegraphics[width=\linewidth]{bosonSampling_scheme.png}
	\caption{
		\textbf{Conceptual boson sampling apparatus.}
		\textbf{(a)} The \emph{input} of the \BosonSampling{} problem is the input many-photon state (in figure the state $\ket{0,0,1,1,0,1,0,0,0}$), and a suitably chosen unitary $U$.
		The \emph{output} is a number of outcomes picked according to the bosonic output probability distribution (in figure, two examples of such states are provided, with \acp{MOL} 101000100 and 110000100).
		Colleting enough such events allows to reconstruct the probability distribution.
		This, however, requires an exponentially increasing (in $n$) number of events.
		\textbf{(b)} Injecting an $m$-mode unitary with $n$ indistinguishable photons, the output state is a weighted superposition of all possible outcomes.
		Measuring in which modes the photons ended up results in the collapsing of this superposition.
		The probability of finding the photons in a certain configuration is given by \cref{eq:BosonSampling_probability_distribution}.
		Credits: \cite{crespi2013integrated}.
	}
	\label{fig:bosonSampling_scheme}
\end{figure}
\clearpage
\begin{example}[Solution of the \textsc{BosonSampling} problem]
	Let $\hat U$ be a randomly chosen unitary transformation, described by the \dimens{4}{4} matrix
	\\

	\resizebox{.9\textwidth}{!}{$
		U = \begin{psmallmatrix}
		 -0.60046+0.220549 i & -0.108966-0.527875 i & -0.367627+0.212122 i & 0.0655053\, +0.340358 i \\
		 -0.576174-0.386133 i & -0.463806+0.524027 i & 0.0458648\, -0.0209767 i & 0.148456\, -0.0679879 i \\
		 -0.0337116-0.30791 i & 0.408837\, -0.0733373 i & -0.0106664+0.578256 i & 0.543236\, -0.319264 i \\
		 -0.0894887-0.0760134 i & 0.211627\, +0.0494216 i & 0.680731\, +0.139364 i & 0.0591935\, +0.672804 i
		\end{psmallmatrix}.
	$}
	\\

	The output probability distribution resulting from the injection of the two-photon input state with MOL $\rstate = (1,1,0,0)$ is
	\begin{center}
		\begin{tabular}{|c|c|c|}
			\hline
			output state & probability amplitude & probability \\
			\hline
			(0,0,0,2) & 0.046478 + 0.0651595 i 	& 0.00640597 \\
			\hline
			(0,0,1,1) & -0.0300108+0.0707215 i  & 0.00590217 \\
			\hline
			(0,0,2,0) & -0.0175525 + 0.0246647 i & 0.000916437 \\
			\hline
			(0,1,0,1) & -0.260804-0.194492 i 	& 0.105846 \\
			\hline
			(0,1,1,0) & 0.0432791 -0.312955 i 	& 0.099814 \\
			\hline
			(0,2,0,0) & 0.462674 + 0.265491 i 	& 0.284553 \\
			\hline
			(1,0,0,1) & 0.0195337 -0.147833 i 	& 0.0222362 \\
			\hline
			(1,0,1,0) & 0.270811 +0.0424448 i 	& 0.0751401 \\
			\hline
			(1,1,0,0) & 0.0218767 -0.0707259 i 	& 0.00548074 \\
			\hline
			(2,0,0,0) & 0.609711 + 0.148185 i 	& 0.393706 \\
			\hline
		\end{tabular}
	\end{center}

	Measuring the exit modes of the two injected photons at each pulse, we obtain a series of \emph{samples} from the above probability distribution.
	An example of 10 such samples is the sequence
	\begin{center}
		\begin{tabular}{c}
			(0,2,0,0) \\
			(0,1,1,0) \\
			(2,0,0,0) \\
			(2,0,0,0) \\
			(0,2,0,0) \\
			(0,1,1,0) \\
			(2,0,0,0) \\
			(0,1,0,1) \\
			(0,2,0,0) \\
			(0,2,0,0)
		\end{tabular}
	\end{center}
	The above list is \emph{exactly} what \textsc{BosonSampling} is all about: obtaining a list of ``labels'' distributed according to a particular probability distribution.

	In other words, the \BosonSampling{} problem \emph{is not} solved obtaining the above listed probabilities, but obtaining a number of samples from this distribution.
	The number of such samples is not really important here, even producing a single state from the correct probability distribution would theoretically be enough to achieve a post-classical computation, though possibly making it harder to experimentally verify.
\end{example}

The hardness of the \textsc{BosonSampling} problem can be traced back to the \sharpP{}-hardness of computing the permanent of a generic complex-valued matrix.
Indeed, as shown in eq. (\ref{eq:scatteringAmplitudeWithPermanent2}), the probability $P(\rstate \rightarrow \sstate, U)$ of an input $\rstate$ evolving into $\sstate$, is proportional to the permanent of the matrix $U[\Rstate | \Sstate]$ (recalling \cref{def:submatrices}):
\begin{equation}
	P(\rstate \rightarrow \sstate, U)
	= \left| \mathcal A ( \rstate \rightarrow \sstate, U) \right|^2
	= \frac{1}{\mu(\Rstate)\mu(\Sstate)}
	\left| \perm (U[\Rstate | \Sstate]) \right|^2.
	\label{eq:BosonSampling_probability_distribution}
\end{equation}
Computing the permanent of a \dimens{n}{n} matrix with the fastest known classical algorithms \cite{glynn2010permanent,minc1984permanents} requires a number of operations of the order $\mathcal O(n 2^n)$.
This means that, for example, computing the permanent of a \dimens{30}{30} complex matrix, corresponding to a single scattering amplitude for a 30-photon state, requires a number of operations of the order of $\sim 10^{10}$.
If the average time required by a classical computer to perform a single operation is of the order $\sim 10^7$, the computation of one such scattering amplitude will require $\sim 10$ minutes.
While still clearly manageable by a classical computer, this already shows the potential advantages of a boson sampling apparatus: if the experimental problems related to coherently evolve 30 indistinguishable photons inside an interferometer were to be solved, this would allow to sample from the probability distribution given by \cref{eq:BosonSampling_probability_distribution} \emph{without actually knowing} the probabilities itselves.

AA demonstrated that, should boson sampling be classically easy to solve, this would have very strong and undesied consequences in computational complexity theory, and therefore it is most probable that boson sampling is not classically easy to solve.

It is worth stressing that the \textsc{BosonSampling} problem \emph{is not} that of finding the permanents in \cref{eq:scatteringAmplitudeWithPermanent2}, but only that of \emph{sampling} from the related probability distribution.
In fact, not even a boson sampler is able to \emph{efficiently} compute these scattering probabilities.
This is due to the fact that, to reconstruct a probability distribution spanning over a number $M$ of events, roughly speaking, the number of samples is required to be at least of the order of $M$.
But, as shown in \cref{eq:number_many-boson_states,eq:number_many-fermion_states}, $M$ scales exponentially with $n$, implying that the number of experimental samples required to reconstruct the probability distribution becomes exceedingly large very soon.
In \cref{fig:boson_sample_varying_samples_m8n2,fig:boson_sample_varying_samples_m8n4} is shown that, if the number of samples is not large enough, the reconstructed probability distribution is different from the real one.
In fact, generally speaking, there are strong arguments against the possibility to compute the permanents of complex-valued matrices by means of quantum experiments \cite{troyansky1996permanent}, although attempts have been reported in this direction \cite{marzuoli2007towards}.

\begin{figure}[tbh!]
    \centering
    \includegraphics[width=.8\linewidth]{bosonSample_4figures_m8n2_exact-10-100-1000.pdf}
    \caption{
    	\textbf{Example of boson sampling from a randomly chosen \dimens{8}{8} unitary matrix.}
    	\textbf{(a)} Exact output probability distribution for two photons injected in the first two modes of a random 8-mode interferometer.
    	\textbf{(b), (c), (d)} Output states sampled from the output probability distribution, for a number of samples equal to 10 \textbf{(b)}, 100 \textbf{(c)} and 1000 \textbf{(d)}.
    	As seen, with a low number of samples, the histogram may appear different from the true probability distribution.
    }
    \label{fig:boson_sample_varying_samples_m8n2}
\end{figure}
\begin{figure}[tbh!]
    \centering
    \includegraphics[width=.8\linewidth]{bosonSample_4figures_m8n4_exact-10-100-1000.pdf}
    \caption{
    	\textbf{Example of boson sampling from a randomly chosen \dimens{8}{8} unitary matrix.}
    	\textbf{(a)} Exact output probability distribution for four photons injected in the first four modes of a random 8-mode interferometer.
    	\textbf{(b), (c), (d)} Output states sampled from the output probability distribution, for a number of samples equal to 10 \textbf{(b)}, 100 \textbf{(c)} and 1000 \textbf{(d)}.
    	The number of samples required to reliably recover the original probability distribution is much higher than in \cref{fig:boson_sample_varying_samples_m8n2}, due to the higher number of many-boson states, which are here $\binom{8+3}{4}=330$ against the $\binom{8+1}{2}=36$ of \cref{fig:boson_sample_varying_samples_m8n2}.
    }
    \label{fig:boson_sample_varying_samples_m8n4}
\end{figure}
\FloatBarrier

The complexity of \BosonSampling{} makes it an extremely valuable candidate to gain experimental evidences of the supremacy of quantum devices over classical computers.
Indeed, it presents several advantages in this regard over, for example, \textsc{Factoring}, which is the paradigmatic problem that would allow quantum computers to perform a post-classical computation:
\begin{enumerate}
	\item The \BosonSampling{} problem is even harder than \textsc{Factoring}, being related to the \sharpPh{} complexity class, and believed to not be in \NP{}.
	\item A boson sampler requires significantly less resources to be implemented than a universal quantum computer.
	In particular, it does not require adaptive or feed-forward mechanisms, nor fault-tolerance methods.
	This relatively simple design has already prompted a number of small-scale implementations of increasing complexity \cite{broome2013photonic,crespi2013integrated,tillmann2013experimental,spring2013boson,spagnolo2013three,spagnolo2014experimental,carolan2014experimental,bentivegna2015experimental,carolan2015universal,crespi2015quantum,spagnolo2013general}.

	AA suggested \cite{aaronson2011computational} that a 400-modes interferometer fed with 20 single photons is already at the boundary of the simulation powers of present-day classical computers.
	While in this regime it would still be possible to carry out a classical simulation, the quantum device should be able to perform the sampling task faster than the classical computer.
	\item The theoretical evidence of the hardness of \BosonSampling{} is stronger than that of factoring integers:
	while in the former case the result only relies on a small number of conjectures regarding the hardness of some problems \autocite{aaronson2011computational}, in the latter case \emph{there is no compelling evidence for \textsc{Factoring} to not be in \Pclass{}}.
	While known to be in \BQP{}, \textsc{Factoring} is only \emph{believed} to be in \NP{}, and \emph{strongly believed} to not be in \nph{}.

	While the hardness of \textsc{Factoring} is strong enough to build modern cryptography, it could also happen that a polynomial-time algorithm will be discovered showing that \textsc{Factoring} is in \Pclass{} as, basically, the sole evidence for its hardness is the fact that no efficient classical algorithm is yet known.
\end{enumerate}

\section{Scaling experimental boson sampling implementations}
\label{sec:scalingExperimentalBosonSamplingImplementations}

The hardness of \BosonSampling{} has another potentially important consequence: it could provide the first experimental evidence \emph{against} the \ac{ECT} \cite{aaronson2011computational}.
This point, however, is still subject to some debate \cite{shchesnovich2014sufficient,shchesnovich2013asymptotic,rohde2014will}, due to the somewhat informal nature of the \ac{ECT} itself.
Indeed, the \ac{ECT} is not a mathematical statement, but a statement about how the physical world behaves, in a certain \emph{asymptotic limit}.
Because of this it is somewhat ill-defined what exactly would be ``enough experimental evidence'' to ``prove'' or ``disprove'' such a statement.

While AA argued for the hardness of both the exact and approximate \BosonSampling{} problems, they did not take into account other forms of experimental imperfections.

More precisely, AA showed that even attempting to \emph{estimate} the output probability distribution of a boson-sampler is likely computationally hard, as long as the probability $P$ of each input state being correctly produced by the sources scales as $P > 1/\operatorname{poly}(n)$, that is, does not vanish faster than the inverse of a polynomial in $n$ \autocite{aaronson2011computational}.
Further evidence that even lossy systems or systems with mode mismatch are likely to be classically hard to solve was later given \autocite{rohde2012error}.

Another source of errors which could potentially undermine the scalability of boson sampling are the unavoidable circuit imperfections, especially taking into account the fact that a boson sampler cannot likely implement fault-tolerant mechanisms \cite{aaronson2011computational}.
Therefore, small errors in the calibrations of the elementary optical elements, like beam splitters and phase-shifters, may accumulate and result in an output probability distribution significantly different from the ideal one.
A thorough analysis of this issue, reported in \cite{leverrier2015analysis}, leads to the result that as long as the average fidelity of the elementary gates scales at least like $1-\mathcal O(1/n^2)$, the overall implementation provides a reasonably good output distribution.

% Other works analysed how much is the \emph{full} indistinguishability of the photons necessary for the hardness of boson sampling \cite{tichy2015sampling,tamma2015multi,tamma2015boson,shchesnovich2014sufficient}.

Generally speaking, all of these works seem to reinforce the idea that the \BosonSampling{} problem \emph{is} scalable even when experimentally plausible conditions are taken into account.
This however, is not definitively enstablished.
While the potential for scaling up the number of modes of network implementations has been fairly well enstablished, and experiments with 2 photons into up to 21 modes have been reported \cite{peruzzo2010quantum}, scaling the number of photons is much harder and will likely require technologically improvements, especially on the side of the employed single-photon sources and detectors.

In particular, most reported boson sampling experiments used \ac{SPDC} single-photon sources.
With this type of scheme, using a single \ac{SPDC} source, the probability of producing $n$ photons decreases exponentially with $n$, as shown in \cref{eq:state_produced_by_SPDC}.
This means that in order to implement a boson sampler with higher numbers of photons another kind of architecture is most likely required.
One such example is provided by a \emph{scattershot boson sampler}, which will be described in the next section.

% \section{Alternative architectures}
% \begin{itemize}
% 	\item \textit{Motes et al, PRL 2014}
% 	\item \textit{Peropadre 2015}
% 	\item ecc.
% \end{itemize}

\section{Scattershot boson sampling}
\label{sec:scattershotBosonSampling}

One of the main difficulties in scaling up the complexity of boson sampling devices is the requirement of a reliable source of many indistinguishable photons.
Indeed, despite recent advances in photon generation \cite{eisaman2011invited} using atoms, molecules, color centers in diamond, and quantum dots, currently the most widely used method remains parametric downconversion.
The drawbacks of \ac{SPDC} sources, described in \cref{sec:single_photon_sources}, have however restricted \ac{SPDC} implementations of boson sampling experiments to proof-of-principle demonstrations.

Recently a new scheme, named \emph{scattershot boson sampling} \cite{aaronson2013new}, has been proposed to make the best use of \ac{SPDC} sources for photonic boson sampling, greatly enchancing the rate of $n$-photon events \cite{lund2014boson,aaronson2013new}.
As opposite to a boson sampling experiment, in which a single fixed input state is employed, in a scattershot boson sampler the photons are injected into randomly chosen modes at each shot.
This is achieved adopting an heralded single-photon source for every mode of the interferometer (see \cref{fig:scattershot_scheme}).
If a single source has a probability $p$ of injecting a photon into the interferometer for each laser pulse, the probability of $n$ particular sources injecting a photon at the same time is $p^n$.
However, there are $\binom{m}{n}$ possible combinations of sources that can simultaneously inject a photon into the interferometer, so that the resulting probability of injection of \emph{any} configuration of $n$ photons is $\binom{m}{n} p^n$.
For $m \gtrsim n^2$, which is the regime in which \BosonSampling{} is hard, the binomial factors dominate over $p^n$.
Thus, while the efficiency of using a single \ac{SPDC} source to generate many photons decreses exponentially with $p^n$, the overall efficiency of a scattershot boson sampling apparatus with $m$ \ac{SPDC} sources \emph{increases} exponentially with $n$, in the regime $n \ll m$, as shown in \cref{fig:scattershot_advantage}.

While a scattershot boson sampling apparatus would technically solve a problem \emph{different} from \BosonSampling{} as defined above, due to the randomly changing input state, it has been shown \cite{aaronson2013new,lund2014boson} that the computational problem solved by a scattershot boson sampler is \emph{at least as hard} as \BosonSampling{}, thus making this new scheme as good as the original one to achieve a post-classical computation.

An experimental implementation of a scattershot boson sampler has been recently reported \cite{bentivegna2015experimental}, using a 13-mode integrated photonic chip and up to six \ac{SPDC} sources, to obtain data corresponding to two- and three-photon interference.

\begin{figure}[tbh!]
	\centering
	\includegraphics[width=\linewidth]{scattershot_scheme.jpg}
	\caption{
		\textbf{A scattershot boson sampler.}
		An \ac{SPDC} source is used for every input of the $m$-mode interferometer.
		Each \ac{SPDC} source generates (ignoring higher order terms) a pair of photons, one of which is injected into the interferometer, while the other \emph{heralds} the generation event.
		Every time a detector corresponding to an heralding photon ``clicks'', we know that a photon has been injected into the corresponding mode of the interferometer.
		(image courtesy of \cite{bentivegna2015experimental})
	}
	\label{fig:scattershot_scheme}
\end{figure}
\begin{figure}[tbh!]
 	\centering
 	\includegraphics[width=\linewidth]{binomBehaviour_and_scattershotAdvantage.pdf}
 	\caption{
 		\textbf{(a)} Logarithmic plot of the binomial factor $\binom{m}{n}$ as a function of $n$, for $m$ equal to 8 (blue), 16 (orange), 32 (green) and 64 (red).
 		\textbf{(b)} Logarithmic plot of the probability $p^n \binom{n^2}{n}$ of injecting $n$ photons into an $m=n^2$-modes interferometer with a scattershot boson sampling apparatus, against the number of photons $n$, with the single pair generation probability $p$ equal to 0.01 (blue), 0.1 (orange) and 0.5 (green).
 		The plot shows how the probability increases exponentially with $n$.
 		Moreover, the lower is the value of $p$, the higher is the number of photons required to see this advantage, as seen here in the case of $p=0.01$.
 		Credits: \cite{bentivegna2015experimental}.
 	}
 	\label{fig:scattershot_advantage}
 \end{figure} 
