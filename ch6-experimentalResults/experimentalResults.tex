%!TEX root = ..\thesis.tex

In this chapter we report the experimental implementation of the protocol devised in \cite{tichy2014stringent,tichy2010zero}, and explained in \cref{sec:fourierSuppressionLaw}, and its use to validate quantum many-body interference against alternative non-trivial hypotheses \cite{crespi2015quantum}.
To this end, a scalable approach for the implementation of the fast Fourier transform algorithm, using 3-D photonic integrated interferometers, is developed.
We observe the suppression law for a large number of output states with 4- and 8-mode optical circuits.
The experimental results demonstrate genuine quantum interference between the injected photons.
This work provides a first experimental realization~\cite{crespi2015quantum} of the validation protocol with Fourier matrices \cite{tichy2014stringent} discussed in \cref{sec:fourierSuppressionLaw} (during this work another experimental realization of the same validation scheme was reported \cite{carolan2015universal}).
The reported experiments were realized in the Quantum Optics laboratories at University of Rome \emph{La Sapienza}.

In \cref{sec:ch6:overview} an overview of the work is given.
In \cref{sec:ch6:femtosecondLaserWriting} the femtosecond laser-writing technique, employed to build the integrated photonic chips used in the experiment, is described.
In \cref{sec:ch6:qFFT} the quantum Fast Fourier Transform is described.
This is a linear optics generalization of the classical Fast Fourier Transform algorithm, used in this work to implement efficiently the Fourier transformation on the integrated interferometer.
In \cref{sec:ch6:photonGenerationAndManipulation} we give a brief description of the photon generation methods.
In \cref{sec:ch6:photonMeasurements} the results of the measurements are presented, focusing on the observation of the Hong-Ou-Mandel dips and peaks, and the reconstruction of the chip transformation.
In \cref{sec:ch6:observationOfSuppressionLaw} we describe how the experimental results validate the genuine many-boson quantum interference occuring in the interferometer, as opposite to alternative models of evolution.
Finally, in \cref{sec:ch6:discussion}, the results of the experiments are discussed and the possible future prospects of this line of research.


\section{Overview of the experiment}
\label{sec:ch6:overview}

We here report the experimental observation of the recently theoretically proposed~\cite{tichy2014stringent} suppression law for Fourier matrices, and its use to validate quantum many-body interference against alternative non-trivial hypotheses resulting in similar output probability distributions.

The Fourier matrices have been implemented with an efficient and reliable approach by exploiting the quantum version of the \ac{qFFT}, an algorithm developed by Barak and Ben-Aryeh \cite{barak2007quantum} to optimize the number of optical elements required to build the Fourier transform over the optical modes.

Here we implement the \ac{qFFT} on photonic integrated interferometers by exploiting the 3-D capabilities of femtosecond laser writing~\cite{osellame2003femtosecond,gattass2008femtosecond}, which makes it possible to fabricate waveguides arranged in three-dimensional structures with arbitrary layouts~\cite{meany2012non,spagnolo2013three,poulios2014quantum}, by adopting an architecture scalable to a larger number of modes.

The observations have been carried out with two-photon Focks states injected into 4- and 8-mode \ac{qFFT} interferometers.
The peculiar behaviour of Fock states compared to other kinds of states is investigated, showing in principle the validity of the certification protocol for the identification of true granular $n$-particle interference, which is the source of a rich landscape of quantum effects such as the computational complexity of \textsc{BosonSampling}.

\section{Femtosecond laser writing}
\label{sec:ch6:femtosecondLaserWriting}

Building large interferometers out of discrete, bulk optical elements tends to result in mechanical instabilities which have prevented the demonstration of even a symmetric, 3-mode interferometer that preserves quantum coherence.
A more promising approach to obtain stable multi-mode interferometers involves the fabrication of this network of linear optical elements by integrated optical waveguides in a glass chip \cite{meany2012non,spagnolo2013three}.

Waveguides are fabricated using the femtosecond laser micromachining technique \cite{gattass2008femtosecond,della2009micromachining}, which exploits nonlinear absorption of focused femtosecond pulses to induce a permanent and localized refractive index increase in transparent materials, as schematically shown in \cref{fig:laser_writing}.
Arbitrary three-dimensional circuits can be directly written by translating the sample along the desired path, keeping the velocity constant with respect to the laser beam.
This technique allows fast and cost-effective prototyping of new devices, enabling the implementation of three-dimensional layouts that are impossible to realize with conventional lithography.

\begin{figure}[tbh!]
    \floatbox[{
        \capbeside\thisfloatsetup{capbesideposition={right,center},capbesidewidth=7cm}
    }]{figure}[\FBwidth]
    {
    	\caption{
        	\textbf{Laser-writing a waveguide on a glass chip.}
        	The waveguide is fabricated focusing femtosecond laser pulses to induce a permanent and localized refractive index increase in transparent materials.
		}
        \label{fig:laser_writing}
    }
    {\includegraphics[width=4cm]{laser_writing.jpg}}
\end{figure}
\FloatBarrier

In the integrated optics approach the role of beam splitters is performed by \emph{directional couplers}, devices which bring two waveguides close together to redistribute the light propagating in them by evanescent field coupling (\cref{fig:integrated_DC_and_PS}-c).
The phase shifters are instead implemented by deforming the S-bent waveguides at the input of each directional coupler in order to stretch the optical path (\cref{fig:integrated_DC_and_PS}-b).
The integrated optics analogue of a discrete components layout, depicted in \cref{fig:evanescentCoupling_vs_bulk}\textbf{-a}, is shown in \cref{fig:evanescentCoupling_vs_bulk}\textbf{-b}, where one can appreciate the one-to-one correspondence between elements in the two approaches.

\begin{figure}[tbph!]
	\centering
	\includegraphics[width=.8\linewidth]{integrated_DC_and_PS.pdf}
	\caption{
		\textbf{Independent control of the phase shift and transmissivity at each directional coupler.}
		\textbf{(a)} The controlled deformation of the S-bent waveguide at the input of each directional coupler and coupling geometry allows independent control over the phase shift and transmissivity.
		\textbf{(b)} The deformation of the S-bent waveguide section is function of a deformation coefficient $d$.
		The graph showed the undeformed S-bend together with a deformed one.
		\textbf{(c)} Control over the transmissivity of the directional coupler is performed by modulating the coupling coefficient.
		This is achieved by changing the waveguide spacing in the coupling region by rotating one arm of the directional coupler out of the main circuit plane.
		A sample dependence of the the transmissivity as a function of the deformation angle is provided.
		Credits: \cite{crespi2013integrated}.
	}
	\label{fig:integrated_DC_and_PS}
\end{figure}
\begin{figure}[tbh!]
	\centering
	\includegraphics[width=.7\linewidth]{evanescentCoupling_vs_bulk.pdf}
	\caption{
		\textbf{Layout of multimode interferometers.}
		\textbf{(a)} Realization of an arbitrary \dimSq{5} mode transformation via a network of beam splitters with different transmissivities $t_i$.
		The blue and red boxes stand for different phase shifters.
		\textbf{(b)} Implementation of the same scheme adopting integrated photonics.
		$\alpha_i$ and $\beta_i$ are parameters characterizing the phase shifts acquired by the photons passing through S-bend waveguide segments.
		Credits: \cite{crespi2013integrated}.
	}
	\label{fig:evanescentCoupling_vs_bulk}
\end{figure}
\FloatBarrier

\section{Realization of the quantum Fast Fourier Transform}
\label{sec:ch6:qFFT}

We here introduce an experimental implementation of the Fourier transformation with the \ac{qFFT} approach.

The general method to realize an arbitrary unitary transformation using linear optics was introduced by Reck et al. \cite{reck1994experimental}, who provided a decomposition of an \dimSq{m} unitary as a sequence of $m(m-1)/2$ beam splitters and phase shifters.
However, in the special case of Fourier matrices, a more efficient method has been proposed \cite{barak2007quantum,torma1996beam}, which takes advantage of their symmetries to significantly reduce the number of linear optical elements required.

Based on the classical algorithm of Cooley and Tukey \cite{cooley1965algorithm}, who first introduced the Fast Fourier Transform algorithm as a more efficient way to compute the discrete Fourier transform, Barak and Ben-Aryeh \cite{barak2007quantum} developed a quantum analogue in the linear optics domain, leading to the concept of \ac{qFFT}.
This approach, valid for $2^p$-dimensional Fourier matrices, requires only $(m/2)\log_2 m$ beam splitters and phase shifters, to be compared with the $\mathcal O(m^2)$ elements needed for the more general Reck decomposition, thus enhancing the compactness and scalability of the platform for a more reliable experimental realization.

The sequential structure arising from the decomposition of the $m$-dimensional Fourier matrix using the Barak and Ben-Aryeh algorithm is reproduced by the consecutive layers shown in \cref{fig:chip_waveguides_3Dscheme}.
The complex arrangement of pairwise interactions necessary for the \ac{qFFT} method cannot be easily implemented using a planar architecture.
However, femtosecond laser writing technique allows to overcome this issue exploiting the third dimension, arranging the waveguides along the bidimensional sections of the integrated chip.

\begin{figure}[tbph!]
	\centering
	\includegraphics[width=\linewidth]{chip_waveguides_3Dscheme.pdf}
	\caption{
		\textbf{Schematic of the structure of the integrated devices.}
		Internal structure of the 4-mode \textbf{(a)} and 8-mode \textbf{(b)} integrated interferometers implementing the \ac{qFFT} over the optical modes.
		The mode arrangement has been chosen in a way to minimize bending losses.
		The insets shows the actual disposition of the waveguides in the cross-section of the devices.
		The modes coupled together in each step $(L_i)$ of the interferometer are joined by segments.
		The implemented phase shifts in each step $(P_i)$ are also indicated.
		Credits: \cite{crespi2015quantum}.
	}
	\label{fig:chip_waveguides_3Dscheme}
\end{figure}
\FloatBarrier

The strategy can be outlined as follows: the $2^p$ modes are ideally placed on the vertices of a $p$-dimensional hypercube, and in each step of the algorithm the vertices connected by parallel edges having one specific direction are made to interact by a 2-mode Hadamard transformation, with proper phase terms.
An optical interferometer implementing this procedure is thus composed of $\log_2 m=p$ sections, each employing $m/2$ balanced beam splitters and phase shifters.

The waveguide interferometers were fabricated realizing the Fourier matrix for $m=4$ and $m=8$ modes in borosilicate glass chips using femtosecond laser micromachining \cite{gattass2008femtosecond,meany2012non}.
A schematic representation of these two interferometers is given in \cref{fig:chip_waveguides_3Dscheme}.
According to the scheme outlined above and by exploiting the three-dimensional capabilities of the fabrication technique, the waveguides are placed, for what concerns the cross section of the device, on the vertices of a 2-D projection of the $p$-dimensional hypercube.
Three-dimensional directional couplers, with proper interaction length and distance to achieve a balanced splitting, connect in each step the required vertices.
The insets of \cref{fig:chip_waveguides_3Dscheme} show, at each step $i$, which modes are connected by directional couplers ($L_i$) and the amount of phase shift that needs to be introduced in specific modes ($P_i$).
Phase shifters, where needed, are implemented by geometrical deformation of the connecting S-bends.
Fan-in and fan-out sections at the input and output of the device allows interfacing with 127-$\mu$m spaced single-mode fiber arrays.

We note that in our device geometry, in each step, the vertices to be connected are all at the same relative distance.
This means that, unless geometric deformations are designed where needed, light travelling in different modes does not acquire undesired phase delays.

The geometric construction here developed is scalable to an arbitrary number of modes with a number of elements increasing as $m \log_2 m$.

\section{Photon generation and manipulation}
\label{sec:ch6:photonGenerationAndManipulation}

The generation of two-photon states is performed by pumping a 2-mm long BBO crystal with the second harmonic of a 785 nm wavelength Ti:Sa pulsed laser, with an average power of 750 mW, which generates photons at 785 nm with a type II parametric downconversion process.
The two photons are spectrally filtered by means of interferential filters with 3 nm full width at half maximum, and coupled into single mode fibers.
The indistinguishability of the photons is then ensured by a polarization compensation stage, and by propagation through independent delay lines before the injection within the interferometer via a single mode fiber array.

After the evolution through the integrated devices, the photons are collected via a multimode fiber array.
The detection system consists of 4 (8) single photon avalanche photodiodes used for the 4-(8-)mode chip.
An electronic data acquisition system allowed us to detect coincidences between all pairs of output modes.
Typical coincidence rates for each collision-free output combination with distinguishable photons amounted to $\sim$70-80 Hz for the 4-mode chip, and $\sim$10-20 Hz for the 8-mode chip.

\section{One- and two-photon measurements in integrated Fourier interferometers}
\label{sec:ch6:photonMeasurements}

The two implemented interferometers of $m=4$ and $m=8$ modes are fed with one- and two-photon states.
The experimental setup, preparing a biphoton wavepacket to be injected into the devices, is shown in \cref{fig:experimental_setup}.

\begin{figure}[tbph!]
	\centering
	\includegraphics[width=.7\linewidth]{experimental_setup.pdf}
	\caption{
		\textbf{Experimental apparatus for input state preparation.}
		\textbf{(a)} The photon source (IF: interferential filter, HWP: half-wave plate, PBS: polarizing beam splitter, PC: polarization compensator, PDC: parametric downconversion, DL: delay lines with motorized stages, SMF: single-mode fiber).
		\textbf{(b)} Photon injection (extraction) before (after) the evolution through the interferometer (FA: fiber array).
		Credits: \cite{crespi2015quantum}.
	}
	\label{fig:experimental_setup}
\end{figure}
\FloatBarrier

To test the validity of the suppression law, we measured the number of coincidences at each forbidden output combination injecting cyclic inputs with two indistinguishable photons.
The degree of violation $\mathcal D$ of the suppression law could simply be evaluated with a counting experiment.
Alternatively, the same quantity $\mathcal D$ can be expressed as a function of single-photon input-output probabilities and of the \ac{HOM} visibilities, defined as
\begin{equation}
	V_{i,j} = \frac{N_{i,j}^D - N_{i,j}^Q}{N_{i,j}^D},
\end{equation}
where $N_{i,j}^D$ is the number of detected coincidences for distinguishable photons, $N_{i,j}^Q$ that for indistinguishable photons, and the subscripts $(i,j)$ are the indices of the two output modes, for a given input state.
The degree of violation can therefore be expressed as:
\begin{equation}
	\mathcal D
	= \frac{N_{\text{forbidden}}}{N_{\text{events}}}
	= P_{\text{forbidden}}
	= \sum_{(i,j)_{\text{forbidden}}} P_{i,j}^Q
	= \sum_{(i,j)_{\text{forbidden}}} P_{i,j}^D (1 - V_{i,j}),
	\label{eq:distinguishabilityVSvisibility}
\end{equation}
where $P_{i,j}^Q$ ($P_{i,j}^D$) are the probabilities of having photons in the outputs $i,j$ in the case of indistinguishable (distinguishable) particles.
Here $P_{i,j}^D$ can be obtained from single-particle probabilities.
The visibilities are measured by recording the number of coincidences for each output combination as a function of the temporal delay between the two injected photons.

For the 4-mode device, we measured the full set of $\binom{4}{2} = 36$ collision-free input-output combinations, that is, those combinations corresponding to the two photons exiting from two different ports.
These contributions have been measured by recording the number of coincidences for each combination of two outputs as a function of the temporal delay between the two input photons.
Due to the suppression law given in \cref{thm:tichy_suppression_law}, we expect to observe four suppressed outcomes (over six possible output combinations) for the two cyclic input states (1,3) and (2,4).
Since distinguishable photons exhibit no interference, \ac{HOM} dips in the coincidence patterns are expected for the suppressed output states.
Conversely, peaks are expected in the non-suppressed output combinations.
The experimental results are shown in \cref{fig:fourier_4modes_onlydips}, where the expected pattern of four suppressions and two enhancements is reported, with average visibilities of $\bar V_{\text{supp}} = 0.899 \pm 0.001$ and $\bar V_{\text{enh}} = -0.951 \pm 0.004$ for suppression and enhancement respectively.

For the cyclic inputs, we also measured the interference patterns for the output contributions where the two photons exit from the same mode.
These terms have been measured by inserting an additional symmetric beam splitter on each output mode, and by connecting each of its two outputs to a single-photon detector.
These cases correspond to a full-bunching scenario with $n=2$, and a \ac{HOM} peak with $V=-1$ visibility is expected independently from the input state and from the unitary operation \cite{spagnolo2013general}.
This feature has been observed for the tested inputs, where an average visibility of $\bar V_{\text{bunch}} = -0.969 \pm 0.024$ has been obtained over all full bunching combinations.

The injection of non-cyclic input states has been employed for the complete reconstruction of the chip action $\tilde U_4^{\text{\ac{qFFT}}}$, using a data set statistically independent from the one adopted to observe the suppression law.
The adopted reconstruction algorithm, which exploits knowledge on the internal structure of the interfometers shown in \cref{fig:chip_waveguides_3Dscheme}, works in two steps.
In a first step, the power-splitting ratios measured with classical light are employed to extrapolate the transmissivities of the directional couplers.
In a second step, the two-photon visibilities for the non-cyclic inputs are used to retrieve the values of the fabrication phases.
In both steps, the parameters are obtained by minimizing a suitable $\chi^2$ function.
The results are shown in \cref{fig:fourier_4modes_chipReconstruction}.
The fidelity between the reconstructed unitary $\tilde U_4^{\text{\ac{qFFT}}}$ and the theoretical Fourier transform $U^F_4$ is $\mathcal F = 0.9822 \pm 0.0001$, thus confirming the high quality of the fabrication process.
The error in the estimation of the fidelity is obtained through a Monte Carlo simulation, properly accounting for the degree of distinguishability of the photons with a rescaling factor in the visibilities.

\begin{figure}[tbph!]
	\centering
	\includegraphics[width=.95\linewidth]{fourier_4modes_onlydips.pdf}
	\caption{
		\textbf{Suppression law in a 4-mode Fourier integrated chip.}
		Complete set of 36 measured coincidence patterns (raw experimental data) for all input-output combinations in the 4-mode chip.
		For each input-output combination, the measured coincidence pattern as a function of the time delay is shown (points: experimental data, lines: best-fit curves).
		Cyclic inputs, highlighted in the picture, exhibit enhancement (green) and suppression (red) on cyclic and non-cyclic outputs, respectively.
		For all points, error bars are due to Poissonian statistics of the events.
		For each visibility, the error is obtained through a Monte Carlo simulation by averaging over 3000 simulated data sets.
		In each plot the zero level coincides with the baseline, while a dashed line represents the number of coincidence events in the distinguishable limit.
		Credits: \cite{crespi2015quantum}.
	}
	\label{fig:fourier_4modes_onlydips}
\end{figure}
\begin{figure}[tbph!]
	\centering
	\includegraphics[width=.8\linewidth]{fourier_4modes_chipReconstruction.pdf}
	\caption{
		\textbf{Suppression law in a 8-mode \ac{qFFT} integrated chip.}
		\textbf{(a)} \ac{HOM} visibilities for all 36 input-output configurations.
		From left to right: experimental measured visibilities ($V_{\text{\ac{qFFT}}}$, obtained from raw experimental data), visibilities calculated from the reconstructed unitary ($V_{\text{rec}}$), and visibilities calculated from the theoretical unitary ($V_{\text{F}}$).
		\textbf{(b)} Representation of the reconstructed experimental transformation $\tilde U_4^{\text{\ac{qFFT}}}$, and comparison with $U_4^{\text{F}}$.
		Colored disks represent the moduli of the reconstructed matrix elements (all equal to $4^{-1/2}$ for $U_4^{\text{F}}$).
		Arrows represent the phases of the unitary matrix elements (green: reconstructed unitary, blue: Fourier matrix).
		Credits: \cite{crespi2015quantum}.
	}
	\label{fig:fourier_4modes_chipReconstruction}
\end{figure}
\FloatBarrier

For the 8-mode chip we recorded all the $\binom{8}{2}= 28$ two-photon coincidence patterns, as a function of the relative delay between the input photons, for each of the 4 collision-free cyclic inputs and for one non-cyclic input.
The results are shown in \cref{fig:fourier_8modes_onlydips}.
The reconstruction of the actual unitary transformation $\tilde U_8^{\text{\ac{qFFT}}}$ implemented has been performed with the same algorithm of the 4-modes, by using the power-splitting ratios measured with classical light and the two-photon visibilities for one non-cyclic input.
The latter has been chosen in a way to maximize the sensitivity of the measurements with respect to the five fabrication phases.
The results are show in \cref{fig:fourier_8modes_chipReconstruction}.
The fidelity between the reconstructed unitary $\tilde U_8^{\text{\ac{qFFT}}}$ and the ideal 8-mode Fourier transform $U_8^F$ is $\mathcal F = 0.9527 \pm 0.0006$.

\begin{figure}[tbph!]
	\centering
	\includegraphics[width=\linewidth]{fourier_8modes_onlydips.pdf}
	\caption{
		\textbf{Suppression law in a 8-mode \ac{qFFT} integrated chip.}
		Set of 28 measured coincidence patterns (raw experimental data), corresponding to all collision-free output combinations for the input (2,6) of the 8-mode interferometer.
		For each output combination, the measured coincidence pattern as a function of the time delay is shown (points: experimental data, lines: best-fit curves).
		Red or green backgrounds correspond to dips and peaks, respectively.
		For all points, error bars are due to the Poissonian statistics of the events.
		For each visibility, the error is obtained through a Monte Carlo simulation by averaging over 3000 simulated data sets.
		In each plot the zero level coincides with the baseline, while a dashed line represents the number of coincidence events in the distinguishable limit.
		Credits: \cite{crespi2015quantum}.
	}
	\label{fig:fourier_8modes_onlydips}
\end{figure}

\begin{figure}[tbph!]
	\centering
	\includegraphics[width=\linewidth]{fourier_8modes_chipReconstruction.pdf}
	\caption{
		\textbf{Suppression law in a 8-mode \ac{qFFT} integrated chip.}
		\textbf{(a)} Average visibilities of dips (red bars) and peaks (green bars) observed for the four collision-free cyclic inputs ((1,5),(2,6),(3,7),(4,8)).
		Darker regions correspond to error bars of $\pm 1$ standard deviation.
		\textbf{(b)} Representation of the reconstructed experimental transformation $\tilde U_8^{\text{\ac{qFFT}}}$, and comparison with $U_8^{\text{F}}$.
		Colored disks represent the moduli of the reconstructed matrix elements (all equal to $8^{-1/2}$ for $U_8^{\text{F}}$).
		Arrows represent the phases of the unitary matrix elements (green: reconstructed unitary, blue: Fourier matrix).
		Credits: \cite{crespi2015quantum}.
	}
	\label{fig:fourier_8modes_chipReconstruction}
\end{figure}

\FloatBarrier
\section{Observation of the suppression law}
\label{sec:ch6:observationOfSuppressionLaw}

The suppression of events which do not satisfy \cref{thm:tichy_suppression_law} is fulfilled only when two perfectly indistinguishable photons are injected in a cyclic input of a perfect Fourier interferometer.
In such a case, we would have the suppression of all output states such that the sum of the elements of the corresponding \ac{MAL} is odd.
For the 4-mode (8-mode) device, this corresponds to 4 (16) suppressed and 2 (12) non-suppressed collision-free outputs (each one given by two possible arrangements of the two distinguishable photons), plus 4 (8) terms with two photons in the same output, each one corresponding to a single possible two-photon path.

The expected violation for distinguishable particles can be obtained from classical considerations.
Let us consider the case with $n=2$.
The two distinguishable photons evolve independently from each other, and the output distribution is obtained by classically mixing single-particle probabilities.
All collision-free terms are equally likely to occur with probability $q=2/m^2$, while full-bunching events occur with probability $q'=q/2=1/m^2$.
The degree of violation $\mathcal D_{\text{D}}$ can then be obtained by multiplying the probability $q$ by the number of forbidden output combinations.
As a result, we expect a violation degree of $\mathcal D_{\text{D}}=0.5$ for distinguishable two-photon states.
The evaluation of the expected value for a mean field state, which is due to single particle bosonic statistic effects, require different calculations \cite{tichy2014stringent}.
It can be shown that for $n=2$ the degree of violation is $\mathcal D_{\text{MF}}=0.25$. 

For each of the cyclic inputs, we have evaluated here the violation degree $\mathcal D_{\text{obs}}$ resulting from collected data.
By measuring the coincidence pattern as a function of the path difference $\Delta x= c \Delta \tau$ between the two photons, and thus by tuning their degree of distinguishability, we could address the transition from distinguishable to indistinguishable particles.
The value of $\mathcal D_{\text{obs}}$ as a function of $\Delta x$ has been obtained as $\sum_{(i,j)_{\text{forbidden}}} P_{i,j}^{\text{D}} ( N_{i,j}^{\Delta x}/N_{i,j}^{\text{D}})$, where $N_{i,j}^{\Delta x}$ and $N_{i,j}^{\text{D}}$ are the number of measured coincidences for a given value of $\Delta x$ and for distinguishable particles respectively.
Two different regions can be identified.
For intermediate values of $\Delta x$ with respect to the coherence length of the photons, the measured data fall below the threshold $\mathcal D_{\text{D}}$, and hence the hypothesis of distinguishable particles can be ruled out.
Then, for smaller values of the path difference up to $\Delta x \rightarrow 0$, true two-photon interference can be certified since both hypothesis of distinguishable particles and mean field state can be ruled out.
The maximum violation occurring at $\Delta x=0$ delay can be evaluated using \cref{eq:distinguishabilityVSvisibility}.
The experimental results retrieved from the protocol are shown in the table of \cref{fig:fourier_validation_zones1,fig:fourier_validation_zones2}, in which we compare the values $\mathcal D_{\text{obs}}(0)$ with the expected values for distinguishable particles $\mathcal D_{\text{D}}$ and for a Mean Field state $\mathcal D_{\text{MF}}$.
As shown for our implementation, the robustness of the protocol is ensured by the high number of standard deviations separating the values in each comparison, thus unambiguously confirming the success of the certification protocol.
In conclusion, the alternative hypotheses of distinguishable particles and of a mean field state can be ruled out for all experiments.

\begin{figure}[tbph!]
	\centering
	\includegraphics[width=\linewidth]{fourier_validation_zones1.pdf}
	% \makebox[\textwidth][c]{\includegraphics[width=1\textwidth]{fourier_validation_zones1.pdf}}%
	\caption{
		Observed violations $\mathcal D_{\text{obs}}$ as a function of the path difference $| \Delta x | = c | \Delta \tau|$ between the two photons, for the 4-mode interferometer.
		Blue shaded regions in the plots correspond to the cases where the hypothesis of distinguishable particles can be ruled out, and true two-particle interference is present.
		Blue points: input (1,3). Red points: input (2,4).
		Blue solid line: theoretical prediction for input (1,3).
		Red solid line: theoretical prediction for input (2,4).
		Black dashed line: theoretical prediction for a Fourier matrix.
		\textbf{(b)} Data for the 8-mode interferometer.
		Blue points: input (1,5). Red points: input (2,6).
		Green points: input (3,7). Magenta points: input (4,8).
		Colored solid lines: corresponding theoretical predictions for different inputs.
		Black dashed line: theoretical prediction for a Fourier matrix.
		Table: violations $\mathcal D_{\text{obs}}(0)$ at $\Delta x=0$ and discrepancies (in sigmas) with the expected values for distinguishable particles ($\mathcal D_{\text{D}}$) and Mean Field Sampler ($\mathcal D_{\text{MF}}$), for the cyclic inputs of the two interferometer.
		$\mathcal D_{\text{obs}}(0)$ are calculated following \cref{eq:distinguishabilityVSvisibility}, while expected values for the other two cases are $\mathcal D_{\text{D}}=0.5$ and $\mathcal D_{\text{MF}}=0.25$.
		Error bars in all experimental quantities are due to the Poissonian statistics of measured events.
		All theoretical predictions in solid lines are calculated from the reconstructed unitaries, obtained from different sets of experimental data to ensure statistical independence.
		Credits: \cite{crespi2015quantum}.
	}
	\label{fig:fourier_validation_zones1}
\end{figure}
\begin{figure}[tbph!]
	\centering
	\includegraphics[width=\linewidth]{fourier_validation_zones2.pdf}
	% \makebox[\textwidth][c]{\includegraphics[width=1\textwidth]{fourier_validation_zones1.pdf}}%
	\caption{
		Observed violations $\mathcal D_{\text{obs}}$ as a function of the path difference $| \Delta x | = c | \Delta \tau|$ between the two photons, for the 8-mode interferometer.
		Blue shaded regions in the plots correspond to the cases where the hypothesis of distinguishable particles can be ruled out, and true two-particle interference is present.
		Blue points: input (1,5). Red points: input (2,6).
		Green points: input (3,7). Magenta points: input (4,8).
		Colored solid lines: corresponding theoretical predictions for different inputs.
		Black dashed line: theoretical prediction for a Fourier matrix.
		Table: violations $\mathcal D_{\text{obs}}(0)$ at $\Delta x=0$ and discrepancies (in sigmas) with the expected values for distinguishable particles ($\mathcal D_{\text{D}}$) and Mean Field Sampler ($\mathcal D_{\text{MF}}$), for the cyclic inputs of the two interferometer.
		$\mathcal D_{\text{obs}}(0)$ are calculated following \cref{eq:distinguishabilityVSvisibility}, while expected values for the other two cases are $\mathcal D_{\text{D}}=0.5$ and $\mathcal D_{\text{MF}}=0.25$.
		Error bars in all experimental quantities are due to the Poissonian statistics of measured events.
		All theoretical predictions in solid lines are calculated from the reconstructed unitaries, obtained from different sets of experimental data to ensure statistical independence.
		Credits: \cite{crespi2015quantum}.
	}
	\label{fig:fourier_validation_zones2}
\end{figure}
\FloatBarrier

\section{Discussion}
\label{sec:ch6:discussion}

We have reported on the experimental observation of the suppression law on specific output combinations of a Fourier transformation due to quantum interference between photons.
The observation of the suppression effect allowed us to rule out alternative hypotheses to the Fock state.
The use of a novel implementation architecture, enabled by the 3-D capabilities of femtosecond laser micromachining, extends the scalability of this technique to larger systems with lower experimental effort with respect to other techniques.
While the presented architecture is designed to implement a Fourier matrix for a number of modes equal to $m=2^p$, a generalization of the approach can be obtained by adopting a building block different from the beam splitter.
For devices of odd dimension, for instance, such a tool can be provided by the tritter transformation \cite{spagnolo2013three}.
At the same time, the universality of a generalized \ac{HOM} effect with an arbitrary number of particles and modes is expected to make it a pivotal tool in the diagnostic and certification of quantum photonic platforms.
\BosonSampling{} represents a key example, since the scalability of the technique is expected to allow efficient certification of devices outperforming their classical counterparts.
An interesting open problem is whether the computational hardness of \BosonSampling{} is mantained if the randomness condition is relaxed, and thus which is the minimal interferometric architecture required for an evidence of post-classical computation.

Fourier matrices can find application in different contexts.
For instance, multiport beam splitters described by the Fourier matrix can be employed as building blocks for multiarm interferometers, which can be adopted for quantum-enhanced single and multiphase estimation protocols \cite{spagnolo2012quantum}.
This would also allow the measurement of phase gradients with precision lower than the shot-noise limit \cite{motes2015linear}.
Other fields where Fourier matrices are relevant include quantum communication scenarios \cite{guha2011structured}, observation of two-photon correlations as a function of geometric phase \cite{laing2012observation}, fundamental quantum information theory including mutually unbiased bases \cite{bengtsson2007mutually}, as well as entanglement generation \cite{lim2005multiphoton}.

% \begin{figure}[tb]
% 	\centering
% 	\includegraphics[scale=.5]{directional_coupler_structure.jpg}
% 	\caption{Caption here}
% 	\label{fig:directional_coupler_structure}
% \end{figure}